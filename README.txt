> ��������� ������
-Code -> C/C++ Code -> Code Generation Options -> Report -> Create code generation report - ���������
-Code -> C/C++ Code -> Code Generation Options -> Templates -> Generate an example main program - ���������
-Code -> C/C++ Code -> Code Generation Options -> Interface -> Shared code placement - ������� Shared location
-Code -> C/C++ Code -> Code Generation Options -> Interface -> Interface - ������� ASAP2
-Code -> C/C++ Code -> Code Generation Options -> Optimization -> Remove internal data zero initialization - ��������� (�������������� ��� ���������� �������� �������� init, �� ������������ �� ������������� � �)
-Code -> C/C++ Code -> Code Generation Options -> Optimization -> Inline invariant signals - �������� (���� ����� ���� near ������, ��������� ������� ����������)

> ���� � ������� ���� ��������� ����� ��������� ����
�������� �� � ����� dependencies � ����� ������

> ����� ��������� � ����������
���� ��������� � ������ ��������� � �������, �� ��������� �� � m. �����:

�������� ��� �������:
K_EXAMPLE = nami.Parameter;
K_EXAMPLE.CoderInfo.StorageClass = 'Custom';
K_EXAMPLE.CoderInfo.CustomStorageClass = 'ExportedConst';

������:
p_EXAMPLE = nami.Signal;
p_EXAMPLE.CoderInfo.StorageClass = 'Custom';
p_EXAMPLE.CoderInfo.CustomStorageClass = 'ExportedFar';

> ���� � ������� ������������ bit ���������� (!!!UPDATE!!!: ���� ������� � ��������������� � ����, �������� ����� editSources)
1. ���������� ��������� genRTWTYPESDOTH.m (����� boolean ��� ��������� ��� _bit � rtwtypes.h).
2. �������� ����������� (����� Matlab �� ����������� boolean � ����� ����������):
Code -> C/C++ Code -> Code Generation Options -> Optimization -> Signals and Parameters -> Pack Boolean data into bitfields