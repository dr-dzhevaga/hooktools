@echo off

set EXECUTION_FOLDER_PATH=%~dp0
call :initWindowSize 150 50 150 20000
call :initColors

:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
::                               Input parameters                                        ::
:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
:: path to origin hex file
if not defined HEX_FILE_PATH (
	set HEX_FILE_PATH=C:\Hook\origin.hex
)

:: path to origin mapping file
if not defined MAPPING_FILE_PATH (
	set MAPPING_FILE_PATH=C:\Hook\origin.map
)

:: model name
if not defined MODEL_NAME (
	set MODEL_NAME=M10_AirFuelPathCtrl
)

:: path to folder with model
if not defined MODEL_FOLDER_PATH (
    set MODEL_FOLDER_PATH=C:\Hook\%MODEL_NAME%
)

:: regular expression for names of variables to reuse with leading '_'
if not defined REUSE_SYMBOLS (
    set REUSE_SYMBOLS="(_(f|p|P|k|K|CW|TK|T|Z|cnt|DT|dt|EC|thh)_.*)|_STARTWMINJ|_FuelConsPulse|_sfpaaa|_TTSHRVMIDLE_TWAT|_ProgramFlowCheck|_TBFLP_RVM"
)

:: name of function to hook ('_' + model name + '_step' or '_initialize')
if not defined HOOK_SYMBOLS (
	set HOOK_SYMBOLS=_%MODEL_NAME%_step,_%MODEL_NAME%_initialize
)

:: name of script to execute before model building (signals and parameters creation script typically)
if not defined MODEL_INIT_SCRIPT_NAME (
    set MODEL_INIT_SCRIPT_NAME=init
)

:: path to file with name conversion table
if not defined SYMBOL_NAME_MAPPING_FILE_PATH (
    set SYMBOL_NAME_MAPPING_FILE_PATH=C:\Hook\nameConversion.txt
)

:: path to file with base linker script
if not defined LINKER_SCRIPT_TEMPLATE_FILE_PATH (
    set LINKER_SCRIPT_TEMPLATE_FILE_PATH=C:\Hook\linkerScriptTemplate.ilo
)

:: output hex file name
if not defined HOOKED_HEX_FILE_NAME (
    set HOOKED_HEX_FILE_NAME=hooked.hex
)

:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
::                               Matlab parameters                                       ::
:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
set MATLAB_PATH=C:\Program Files\MATLAB\R2014b\bin\matlab.exe
set MATLAB_OPTIONS=-nojvm -nodisplay -nosplash -nodesktop -wait
set MATLAB_LOG_FILE_NAME=matlab.log
set MATLAB_SCRIPT="try,diary('%MATLAB_LOG_FILE_NAME%');%MODEL_INIT_SCRIPT_NAME%;rtwbuild('%MODEL_NAME%');catch e,display(getReport(e));exit(1);end;exit(0);"
set MATLAB_OUTPUT_FOLDER=%MODEL_FOLDER_PATH%\%MODEL_NAME%_ert_rtw

:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
::                               Tasking parameters                                      ::
:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
set TASKING_VERSION=XE

set TASKING_LINKER_SCRIPT_FILE_NAME=linkerScript.ilo
set TASKING_MAPPING_FILE_NAME=%MODEL_NAME%.map
set TASKING_HEX_FILE_NAME=%MODEL_NAME%.hex

set TASKING_XE_PATH=C:\Program Files (x86)\TASKING\c166 v8.7r1\bin\cc166.exe
set TASKING_XE_COMPILER_OPTIONS=-Ms -Wc-x2 -Wc-Bhoeufmknladij -Wc-Ob -Wc-znocustack -Wc-zautobitastruct-4 -Wc-zautobita-0 -noc++ -Wc-zvolatile_union -Wc-O1 -Wc-newerr -tmp -Wc-i0
set TASKING_XE_ASSEMBLER_OPTIONS=-WaSB -WaXR -WaPL(60) -WaPW(120) -WaTA(8) -WaWA(1) -WaNOM166 -WaSN(regxe164f.def) -WaEXTEND2
set TASKING_XE_LINKER_OPTIONS=-WoPRINT("%TASKING_MAPPING_FILE_NAME%") -Ms -x2 EXTEND2 -Bhoeufmknladij  -cf "%TASKING_LINKER_SCRIPT_FILE_NAME%" -o %TASKING_HEX_FILE_NAME% -ihex -v -tmp

set TASKING_VX_PATH=C:\Program Files (x86)\C166-VX v3.1r2\bin\cc166.exe
set TASKING_VX_COMPILER_OPTIONS=-Cxc2797x_200f -t -Wa-gAHLS --emit-locals=-equ,-symbols -Wa-OgsaJ -Wa--error-limit=42 -Mn --iso=99 --language=+cmp14,+div32,-gcc,-volatile,+strings -O2 --tradeoff=4 --compact-max-size=200 --source --global-type-checking
set TASKING_VX_LINKER_OPTIONS=-Cxc2797x_200f -t -Mn -Wl-o"%TASKING_HEX_FILE_NAME%":IHEX:4 --hex-format=s -Wl-OtZxYCL -Wl--map-file="%TASKING_MAPPING_FILE_NAME%":XML -Wl-mcrfiklSmNduQ -Wl--error-limit=42 -g --global-type-checking -Wl-DXC2797X_200F "%TASKING_LINKER_SCRIPT_FILE_NAME%"

:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
::                               Java parameters                                         ::
:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
set JAVA_HOME=C:\Program Files (x86)\Java\jre1.8.0_60
set LINKER_SCRIPT_INJECTOR=ru.nami.hooktools.LinkerScriptInjector
set HEX_ASM_INJECTOR=ru.nami.hooktools.HexAsmInjector
set HEX_MERGER=ru.nami.hooktools.HexMerger
set REGEX_REPLACER=ru.nami.hooktools.RegexReplacer

:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
::                               Execution flow                                          ::
:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
call :clear
call :generateSources
call :editSources
call :moveSharedSources
call :copyDependencies
call :generateLinkerScript
call :build
call :inject
call :merge
call :exitWithSuccess

:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
::                               Execution flow functions                                ::
:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
:clear :: Clear output
call :printInfo "Clearing output folder..."
rmdir /Q /S "%MATLAB_OUTPUT_FOLDER%" >nul 2>&1
call :checkErrorLevel
goto :eof

:generateSources ::Generate sources
call :printInfo "Generating sources from matlab model..."
cd "%MODEL_FOLDER_PATH%"
if exist %MATLAB_LOG_FILE_NAME% (
	del %MATLAB_LOG_FILE_NAME% /Q
)
"%MATLAB_PATH%" %MATLAB_OPTIONS% -r %MATLAB_SCRIPT%
call :saveErrorLevel
if exist %MATLAB_LOG_FILE_NAME% (
	type %MATLAB_LOG_FILE_NAME%
)
call :checkSavedErrorLevel
goto :eof

:moveSharedSources
call :printInfo "Coping source files from shared location..."
copy /Y "%MODEL_FOLDER_PATH%\slprj\ert\_sharedutils\*.c" "%MATLAB_OUTPUT_FOLDER%"
copy /Y "%MODEL_FOLDER_PATH%\slprj\ert\_sharedutils\*.h" "%MATLAB_OUTPUT_FOLDER%"
call :checkErrorLevel
goto :eof

:editSources
call :printInfo "Editing model sources..."
java -cp %EXECUTION_FOLDER_PATH%\bin\hookTools.jar %REGEX_REPLACER% --input "%MATLAB_OUTPUT_FOLDER%\%MODEL_NAME%.h" --output "%MATLAB_OUTPUT_FOLDER%\%MODEL_NAME%.h" --line-pattern "extern boolean_T f_\d{1,3}_\w+;" --replace-pattern "boolean_T" --replace-value "bit"
call :checkErrorLevel
java -cp %EXECUTION_FOLDER_PATH%\bin\hookTools.jar %REGEX_REPLACER% --input "%MATLAB_OUTPUT_FOLDER%\%MODEL_NAME%.c" --output "%MATLAB_OUTPUT_FOLDER%\%MODEL_NAME%.c" --line-pattern "const \w+ \w+ = \d+\w*;" --replace-pattern " = \d+\w*"
call :checkErrorLevel
java -cp %EXECUTION_FOLDER_PATH%\bin\hookTools.jar %REGEX_REPLACER% --input "%MATLAB_OUTPUT_FOLDER%\%MODEL_NAME%.c" --output "%MATLAB_OUTPUT_FOLDER%\%MODEL_NAME%.c" --line-pattern "\w{1,2}_%MODEL_NAME%_T %MODEL_NAME%_\w{1,2}(;| = \{)" --replace-pattern "\w{1,2}_%MODEL_NAME%_T" --replace-value "far $0"
call :checkErrorLevel
java -cp %EXECUTION_FOLDER_PATH%\bin\hookTools.jar %REGEX_REPLACER% --input "%MATLAB_OUTPUT_FOLDER%\%MODEL_NAME%.h" --output "%MATLAB_OUTPUT_FOLDER%\%MODEL_NAME%.h" --line-pattern "extern \w{1,2}_%MODEL_NAME%_T %MODEL_NAME%_\w{1,2};" --replace-pattern "\w{1,2}_%MODEL_NAME%_T" --replace-value "far $0"
call :checkErrorLevel
goto :eof

:copyDependencies
call :printInfo "Coping model dependencies..."
copy /Y "%MODEL_FOLDER_PATH%\dependencies\*" "%MATLAB_OUTPUT_FOLDER%"
call :checkErrorLevel
goto :eof

:generateLinkerScript ::Generate linker script
call :printInfo "Generating linker script..."
copy /Y "%LINKER_SCRIPT_TEMPLATE_FILE_PATH%" "%MATLAB_OUTPUT_FOLDER%\%TASKING_LINKER_SCRIPT_FILE_NAME%"
call :checkErrorLevel
java -cp %EXECUTION_FOLDER_PATH%\bin\hookTools.jar %LINKER_SCRIPT_INJECTOR% --origin-mapping "%MAPPING_FILE_PATH%" --linker-script-template "%MATLAB_OUTPUT_FOLDER%\%TASKING_LINKER_SCRIPT_FILE_NAME%" --import-symbols %REUSE_SYMBOLS% --name-mapping "%SYMBOL_NAME_MAPPING_FILE_PATH%"
call :checkErrorLevel
goto :eof

:build
if %TASKING_VERSION%==XE (
	call :build_xe
) else if %TASKING_VERSION%==VX (
	call :build_vx
) else (
	call :printError "Unknown tasking version is specified"
	call :exitWithError
)
goto :eof

:build_xe
cd "%MATLAB_OUTPUT_FOLDER%"
call :printInfo "Building..."
"%TASKING_XE_PATH%" %TASKING_XE_COMPILER_OPTIONS% %TASKING_XE_ASSEMBLER_OPTIONS% %TASKING_XE_LINKER_OPTIONS% *.c
call :checkErrorLevel
goto :eof

:build_vx
cd "%MATLAB_OUTPUT_FOLDER%"
call :printInfo "Compiling..."
"%TASKING_VX_PATH%" %TASKING_VX_COMPILER_OPTIONS% *.c
call :checkErrorLevel
call :printInfo "Linking..."
"%TASKING_VX_PATH%" %TASKING_VX_LINKER_OPTIONS% *.obj
call :checkErrorLevel
goto :eof

:inject ::Inject hook address to calls
call :printInfo "Injecting hook..."
cd "%MATLAB_OUTPUT_FOLDER%"
copy "%HEX_FILE_PATH%" "%HOOKED_HEX_FILE_NAME%" >nul
java -cp %EXECUTION_FOLDER_PATH%\bin\hookTools.jar %HEX_ASM_INJECTOR% --origin-hex "%HOOKED_HEX_FILE_NAME%" --origin-mapping "%MAPPING_FILE_PATH%" --hook-mapping "%TASKING_MAPPING_FILE_NAME%" --hook-symbols %HOOK_SYMBOLS%
call :checkErrorLevel
goto :eof

:merge ::Merge origin and hook hex files
call :printInfo "Merging hex files..."
cd "%MATLAB_OUTPUT_FOLDER%"
java -cp %EXECUTION_FOLDER_PATH%\bin\hookTools.jar %HEX_MERGER% --origin-hex "%HOOKED_HEX_FILE_NAME%" --patch-hex "%TASKING_HEX_FILE_NAME%" --overlap-strategy override --silent-mode
call :checkErrorLevel
goto :eof

:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
::                                 Util functions                                        ::
:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
:printInfo :: Print info string with green color
call :printColoredText green black %1
goto :eof

:printError :: Print error string with green color
call :printColoredText red black %1
goto :eof

:checkErrorLevel :: Print error in case of last call fail
if ERRORLEVEL 1 (
	call :exitWithError
)
goto :eof

:saveErrorLevel :: Print error in case of last call fail
if ERRORLEVEL 1 (
	set SAVED_ERROR_LEVEL=1
) else (
	set SAVED_ERROR_LEVEL=0
)
goto :eof

:checkSavedErrorLevel :: Print error in case of last call fail
if %SAVED_ERROR_LEVEL%==1 (
	call :exitWithError
)
goto :eof

:exitWithError
echo.
call :printError "Hooking is failed!"
pause
exit

:exitWithSuccess
echo.
call :printInfo "Hooked successfully!"
pause
exit

:printColoredText :: Print string with specified color
powershell -command write-host -foreground "%~1" -background "%~2" "%~3"
goto :eof

:initColors :: Some magic for colored printing
SETLOCAL EnableDelayedExpansion
for /F "tokens=1,2 delims=#" %%a in ('"prompt #$H#$E# & echo on & for %%b in (1) do rem"') do (
  set "DEL=%%a"
)
goto :eof

:initWindowSize  winWidth  winHeight  bufWidth  bufHeight :: Set window size
mode con: cols=%1 lines=%2
powershell -command "&{$H=get-host;$W=$H.ui.rawui;$B=$W.buffersize;$B.width=%3;$B.height=%4;$W.buffersize=$B;}"
goto :eof