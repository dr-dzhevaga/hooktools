//
//  Linker script file for the VX-toolset for C166
//

// Define the near page addresses. Each DPP will point to a near page.
// It is recommended to keep __DPP3_ADDR at 0x00C000

#define __SYSTEM_STACK_SIZE	1
#define __USER_STACK_SIZE	1
#define __NHEAP_SIZE		1
#define __HHEAP_SIZE		1

#define	__DPP0_ADDR	0xC84000
#define	__DPP1_ADDR	0xC80000
#define	__DPP2_ADDR	0xC88000
#define	__DPP3_ADDR	0x00C000 

#if defined(__CPU_XC2797X__)
#include "xc2797x.lsl"
derivative my_xc2797x extends xc2797x
{

}
#else
#include <cpu.lsl>
#endif
