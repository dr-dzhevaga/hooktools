function [ansiDataTypeName] = genRTWTYPESDOTH(hardwareImp, hardwareDeploy, configInfo, simulinkInfo)
%GENRTWTYPESDOTH Create rtwtypes.h based on hardware characteristics and
%additional configuration information. Returns appropriate data types names
%for the standard MATLAB types.
%
% NOTE: This function does not require a model to be opened.
%
% ARGUMENTS:
%
% hardwareImp
%     CharNumBits                   = int32(8);
%     ShortNumBits                  = int32(16);
%     IntNumBits                    = int32(32);
%     LongNumBits                   = int32(32);
%     LongLongNumBits               = int32(64);
%     WordSize                      = int32(32);
%     ShiftRightIntArith            = true;
%     LongLongMode                  = false;
%     IntDivRoundTo                 = 'Floor'; % 'Floor', 'Zero' or 'Undefined'
%     Endianess                     = 'LittleEndian'; % 'LittleEndian', 'BigEndian' or 'Unspecified'
%     HWDeviceType                  = 'Generic->32-bit Embedded Processor';
%     TypeEmulationWarnSuppressLevel= 0      (optional)
%     PreprocMaxBitsSint            = 32;    (optional)
%     PreprocMaxBitsUint            = 32;    (optional)
%    
% hardwareDeploy
%     CharNumBits                   = int32(8);
%     ShortNumBits                  = int32(16);
%     IntNumBits                    = int32(32);
%     LongNumBits                   = int32(32);
%     LongLongNumBits               = int32(64);
%     LongLongMode                  = false;
%
% configInfo
%     GenDirectory                  = '/temp/slprj/ert/_sharedutils/'
%     PurelyIntegerCode             = false;
%     SupportComplex                = true;
%     MaxMultiwordBits              = 2048;
%
% simulinkInfo                      (optional, set to [] if unused)
%     Style                         = 'minimized'; % 'full' or 'minimized'
%     SharedLocation                = false;
%     IsERT                         = true;
%     PortableWordSizes             = false;
%     SupportNonInlinedSFcns        = false;
%     GRTInterface                  = false;
%     ReplacementTypesOn            = false;
%     ReplacementTypesStruct        = [];
%     DesignDataLocation            = 'base';
%     GenChunkDefs                  = false;
%     GenBuiltInDTEnums             = false;
%     GenTimingBridge               = false; 
%     GenErtSFcnRTWTypes            = false;

%   Copyright 2003-2014 The MathWorks, Inc.
%   
%   

    if isempty(simulinkInfo)
        %Setup defaults
        usingSimulink = false;
        simulinkInfo.Style = 'minimized';
        simulinkInfo.SharedLocation = false;
        simulinkInfo.IsERT = true;
        simulinkInfo.PortableWordSizes = false;
        simulinkInfo.SupportNonInlinedSFcns = false;
        simulinkInfo.GRTInterface = false;
        simulinkInfo.ReplacementTypesOn = false;
        simulinkInfo.ReplacementTypesStruct = [];
        simulinkInfo.DesignDataLocation = 'base';
        simulinkInfo.GenChunkDefs = false;
        simulinkInfo.GenBuiltInDTEnums = false;
        simulinkInfo.GenTimingBridge = false;
        simulinkInfo.GenErtSFcnRTWTypes = false;
    else
        usingSimulink = true;
    end
    locCheckArgs(hardwareImp, hardwareDeploy, configInfo, simulinkInfo)
    
    % Create the Ansi datatype table
    [ansiDataTypeTable, ansiDataTypeName] = rtwprivate('initAnsiDataType', ...
                                                      hardwareDeploy, hardwareImp, ...
                                                      configInfo.PurelyIntegerCode, ...
                                                      simulinkInfo.ReplacementTypesOn, ...
                                                      simulinkInfo.ReplacementTypesStruct, ...
                                                      simulinkInfo.DesignDataLocation);

    % Include COMPLEX number typedefs if select
    GEN_CMPLX_TYPES = configInfo.SupportComplex;
    
    % Include model reference typedefs if select 
    GEN_MDLREF_TYPES = simulinkInfo.GenTimingBridge;
    
    % Include builtin datatype enums if select
    GEN_BUILTIN_TYPEID_TYPES = simulinkInfo.GenBuiltInDTEnums;
           
    ENABLE_PORTABLE_WORDSIZES = simulinkInfo.PortableWordSizes;
            
    % Include long long on typedefs
    ENABLE_LONG_LONG = rtwprivate('getAnsiDataType',ansiDataTypeTable,'SupportLongLong');

    if (ENABLE_LONG_LONG == 1)
        ansi_longlong = rtwprivate('getAnsiDataType',ansiDataTypeTable, 'tSS_LONG_LONG');
        chunk_size = double(ansi_longlong.numBits);
    else
        ansi_long = rtwprivate('getAnsiDataType',ansiDataTypeTable, 'tSS_LONG');
        chunk_size = double(ansi_long.numBits);
    end
        
    style = simulinkInfo.Style;
    MaxMultiwordBits = configInfo.MaxMultiwordBits;

    GEN_MULTIWORD_TYPES = simulinkInfo.GenChunkDefs || ...
    (MaxMultiwordBits > chunk_size);
    
    % skip update rtwtypes.h operation if it already exists (incremental code gen)
    if simulinkInfo.GenErtSFcnRTWTypes
        tmwtypesFile = fullfile(configInfo.GenDirectory,'rtwtypes_sf.h');
    else
        tmwtypesFile = fullfile(configInfo.GenDirectory,'rtwtypes.h');
    end
    rtwtypesChecksumFile = fullfile(configInfo.GenDirectory,'rtwtypeschksum.mat');
    ansiDataTypeName.multiword_types = GEN_MULTIWORD_TYPES;
    ansiDataTypeName.model_reference_types = GEN_MDLREF_TYPES;
    ansiDataTypeName.builtin_typeid_types = GEN_BUILTIN_TYPEID_TYPES;
    ansiDataTypeName.overwrittenrtwtypes = false;
    ansiDataTypeName.overwrittencmplxTypes = false;
    ansiDataTypeName.overwrittenmdlrefTypes = false;
    ansiDataTypeName.overwrittenbuiltinTypeidTypes = false;
    ansiDataTypeName.overwrittenmultiwordTypes = false;
    mdlName = '';
    if isfield(configInfo, 'ModelName') && ~isempty(configInfo.ModelName)    
        mdlName = configInfo.ModelName;
    end    
    complexAlignInfo = locGetComplextypeAlignmentInfo(mdlName, hardwareImp, hardwareDeploy);
    % cache additional alignment info
    if ~isempty(mdlName)
        complexAlignInfo.fieldDAF = rtwprivate('da_support', mdlName, 'DATA_ALIGNMENT_STRUCT_FIELD');
        complexAlignInfo.structDAF = rtwprivate('da_support', mdlName, 'DATA_ALIGNMENT_WHOLE_STRUCT');
    else
        complexAlignInfo.fieldDAF = false;
        complexAlignInfo.structDAF = false;
    end

    % Initially assume we overwrite all coder files when their component exists
    overwriteCoderTypes = true;
    overwriteTimingBridge = GEN_MDLREF_TYPES;
    overwriteGenBuiltInDTEnums = GEN_BUILTIN_TYPEID_TYPES;
    overwriteMultiwordTypes = GEN_MULTIWORD_TYPES;

    % Then if we are sharing and have generated code before, we can optimize
    % their existence in some cases
    if exist(tmwtypesFile,'file') && simulinkInfo.SharedLocation  
        savedrtwtypeshchksum = load(rtwtypesChecksumFile);

        % Set the attributes related to loaded past settings to false
        overwriteStyle = false;
        overwriteSupportComplex = false;
        overwriteMaxMultiwordBits = false;
        overwriteGenChunkDefs = false;
        overwriteGenBuiltInDTEnums = false;
        overwriteTimingBridge = false;
        
        % When previously generated style is minimized but current style is full,
        % rtwtypes.h needs to be overwritten. Reasons for this can include: GRTInterface
        % is on or NumChildSfcn > 0.
        if strcmpi(simulinkInfo.Style,'full') && ...
                ~strcmpi(simulinkInfo.Style, savedrtwtypeshchksum.simulinkInfo.Style)
            overwriteStyle = true;
        end
        
        % When previously generated does not support complex but current needs,
        % rtwtypes.h needs be overwritten.
        if ~savedrtwtypeshchksum.configInfo.SupportComplex && GEN_CMPLX_TYPES
            overwriteSupportComplex = true;
        end
        
        % When previously generated multiword max length is less than currently
        % needed multiword max length, multiword_types.h needs to be overwritten.
        if savedrtwtypeshchksum.configInfo.MaxMultiwordBits < configInfo.MaxMultiwordBits
            overwriteMaxMultiwordBits = true;
        end
        
        % When previously generated does not support GenChunkDefs but current needs,
        % multiword_types.h needs be overwritten.
        if ~savedrtwtypeshchksum.simulinkInfo.GenChunkDefs && simulinkInfo.GenChunkDefs 
            overwriteGenChunkDefs = true;  
        end
        
        % If the max multiword bits has increased, or overwriting complex types,
        % or status of writing chunk defs has changed, overwrite the multiword types file
        overwriteMultiwordTypes = overwriteMaxMultiwordBits || ...
            overwriteSupportComplex || overwriteGenChunkDefs;

        % When previously generated does not support GenBuiltInDTEnums but current needs,
        % coder_builtin_typeif_types.h needs be overwritten.
        if ~savedrtwtypeshchksum.simulinkInfo.GenBuiltInDTEnums && simulinkInfo.GenBuiltInDTEnums 
            overwriteGenBuiltInDTEnums = true;  
        end
        
        % If the previous builds did not need a timing bridge, but current needs it,
        % model_reference_types.h needs to be overwritten.
        if ~savedrtwtypeshchksum.simulinkInfo.GenTimingBridge && simulinkInfo.GenTimingBridge
            overwriteTimingBridge = true;  
        end
        
        % If the style or complexity has changed, overwrite the base types file
        overwriteCoderTypes = overwriteStyle || overwriteSupportComplex;
        
        % No need to overwrite any of the coder types files
        if ~overwriteCoderTypes && ...
                ~overwriteTimingBridge && ...
                ~overwriteGenBuiltInDTEnums && ...
                ~overwriteMultiwordTypes
            return;
        end
        
        ansiDataTypeName.overwrittenrtwtypes = overwriteCoderTypes;
        ansiDataTypeName.overwrittenmdlrefTypes = overwriteTimingBridge;
        ansiDataTypeName.overwrittenbuiltinTypeidTypes = overwriteGenBuiltInDTEnums;
        ansiDataTypeName.overwrittenmultiwordTypes = overwriteMultiwordTypes;
        
        % Use and capture highwater marks
        if ~overwriteStyle
            style = savedrtwtypeshchksum.simulinkInfo.Style;
            simulinkInfo.Style = style;
        end
        if ~overwriteSupportComplex
            GEN_CMPLX_TYPES = savedrtwtypeshchksum.configInfo.SupportComplex;
            configInfo.SupportComplex = GEN_CMPLX_TYPES;
        end
        if ~overwriteMaxMultiwordBits
            MaxMultiwordBits = savedrtwtypeshchksum.configInfo.MaxMultiwordBits;
            configInfo.MaxMultiwordBits = MaxMultiwordBits;
        end
        if ~overwriteGenChunkDefs
            simulinkInfo.GenChunkDefs =  savedrtwtypeshchksum.simulinkInfo.GenChunkDefs;
        end
        if ~overwriteGenBuiltInDTEnums
            simulinkInfo.GenBuiltInDTEnums = savedrtwtypeshchksum.simulinkInfo.GenBuiltInDTEnums;
        end
        if ~overwriteTimingBridge
            GEN_MDLREF_TYPES = savedrtwtypeshchksum.simulinkInfo.GenTimingBridge;
            simulinkInfo.GenTimingBridge = GEN_MDLREF_TYPES;
        end

    end

    f = -1;
    mf = -1;
    tf = -1;
    wf = -1;
    try
        save(rtwtypesChecksumFile, 'hardwareImp', 'hardwareDeploy', 'configInfo', 'simulinkInfo');
        
        % get the ANSI data types, and some other related info such as native word
        % size and floating point support.
        ansi_int8          = rtwprivate('getAnsiDataType',ansiDataTypeTable,'tSS_INT8');
        if isempty(ansi_int8.val)
            DAStudio.error('RTW:buildProcess:missingRequiredDataType',...
                           '"int8"', '8-bits, 16-bits or 32-bits');
        end
        ansi_uint8         = rtwprivate('getAnsiDataType',ansiDataTypeTable,'tSS_UINT8');
        if isempty(ansi_uint8.val)
            DAStudio.error('RTW:buildProcess:missingRequiredDataType',...
                           '"uint8"', '8-bits, 16-bits or 32-bits');
        end

        ansi_int16         = rtwprivate('getAnsiDataType',ansiDataTypeTable,'tSS_INT16');
        if isempty(ansi_int16.val)
            DAStudio.error('RTW:buildProcess:missingRequiredDataType',...
                           '"int16"', '16-bits or 32-bits');
        end
        ansi_uint16        = rtwprivate('getAnsiDataType',ansiDataTypeTable,'tSS_UINT16');
        if isempty(ansi_uint16.val)
            DAStudio.error('RTW:buildProcess:missingRequiredDataType',...
                           '"uint16"', '16-bits or 32-bits');
        end

        ansi_int32         = rtwprivate('getAnsiDataType',ansiDataTypeTable,'tSS_INT32');
        if isempty(ansi_int32.val)
            DAStudio.error('RTW:buildProcess:missingRequired32DataType', '"int32"');
        end
        ansi_uint32        = rtwprivate('getAnsiDataType',ansiDataTypeTable,'tSS_UINT32');
        if isempty(ansi_uint32.val)
            DAStudio.error('RTW:buildProcess:missingRequired32DataType', '"uint32"');
        end

        ansi_int64         = rtwprivate('getAnsiDataType',ansiDataTypeTable,'tSS_INT64');
        ansi_uint64        = rtwprivate('getAnsiDataType',ansiDataTypeTable,'tSS_UINT64');

        ansi_real64        = rtwprivate('getAnsiDataType',ansiDataTypeTable,'tSS_DOUBLE');
        ansi_real32        = rtwprivate('getAnsiDataType',ansiDataTypeTable,'tSS_SINGLE');

        ansi_boolean       = rtwprivate('getAnsiDataType',ansiDataTypeTable,'tSS_BOOLEAN');

        ansi_support_fp    = rtwprivate('getAnsiDataType',ansiDataTypeTable,'SupportFloatingPoint');
        
        ideal_ansi_sizes   = all([ansi_int8.native_support...
                            ansi_int16.native_support...
                            ansi_int32.native_support]);
                        
        ReplaceTypesIncludeHdr = '';
        ReplaceRealTypeDefs = '';
        ReplaceImagTypeDefs = '';
             
        % if non-ERT code format, simply include tmwtypes.h and simstruc_types.h
        if strcmpi(style,'full')

            if overwriteCoderTypes
                f=fopen(tmwtypesFile, 'wt');
                fprintf(f, '%s\n', '');
                fprintf(f, '%s\n', '#ifndef __RTWTYPES_H__  ');
                fprintf(f, '%s\n', '  #define __RTWTYPES_H__  ');
                fprintf(f, '%s\n', '  #include "tmwtypes.h"  ');
                fprintf(f, '%s\n', '  ');
                fprintf(f, '%s\n', '  #include "simstruc_types.h"  ');
                fprintf(f, '%s\n', '    #ifndef POINTER_T  ');
                fprintf(f, '%s\n', '    # define POINTER_T  ');
                fprintf(f, '%s\n', '      typedef void * pointer_T;  ');
                fprintf(f, '%s\n', '    #endif  ');
                fprintf(f, '%s\n', '/* Logical type definitions */  ');
                fprintf(f, '%s\n', '#if (!defined(__cplusplus))  ');
                fprintf(f, '%s\n', '#  ifndef false');                                     
                fprintf(f, '%s\n', '#   define false (0U)');                                
                fprintf(f, '%s\n', '#  endif');                                            
                fprintf(f, '%s\n', '#  ifndef true   ');                                   
                fprintf(f, '%s\n', '#   define true (1U)');                                 
                fprintf(f, '%s\n', '#  endif  ');                                          
                fprintf(f, '%s\n', '#endif');                     

                if simulinkInfo.ReplacementTypesOn
                    [ReplaceTypesIncludeHdr, ...
                     ReplaceRealTypeDefs, ...
                     ReplaceImagTypeDefs] = loc_get_replacement_typedefs(...
                         GEN_CMPLX_TYPES, simulinkInfo, ...
                         true, true, true, true, true, true, true, true, true);
                end

                if simulinkInfo.ReplacementTypesOn && ...
                        (~isempty(ReplaceTypesIncludeHdr) || ...
                        ~isempty(ReplaceRealTypeDefs))
                    fprintf(f, '%s\n', '  ');
                    fprintf(f, '%s\n', '/* Define Simulink Coder replacement data types. */  ');
                    fprintf(f, ReplaceTypesIncludeHdr);
                    fprintf(f, '%s\n', '  ');
                    fprintf(f, ReplaceRealTypeDefs);
                    fprintf(f, '%s\n', '  ');
                end
                
                % emit additional typedef's
                dumpAdditionalTypeDefs(f, ansiDataTypeTable, true, [], false, [], ENABLE_LONG_LONG);
            
                % emit additional complex types
                if GEN_CMPLX_TYPES
                    dumpAdditionalComplexTypes(...
                        f, ansiDataTypeTable, true, true, ...
                        ENABLE_LONG_LONG, mdlName, complexAlignInfo, ...
                        ReplaceImagTypeDefs);
                end

                fprintf(f, '%s\n', '#endif /* __RTWTYPES_H__ */  ');
                
                fclose(f);
                c_beautifier(tmwtypesFile);
            end
            
            if GEN_MULTIWORD_TYPES && overwriteMultiwordTypes
                multiwordTypesFile = fullfile(configInfo.GenDirectory,'multiword_types.h');
                wf=fopen(multiwordTypesFile, 'wt');
                dumpMultiWordTypeDefs(wf, ...
                                      simulinkInfo.GenChunkDefs, ...                                                                    
                                      'chunks', ...
                                      GEN_CMPLX_TYPES, ...
                                      MaxMultiwordBits, ...
                                      [], ...
                                      false, ...
                                      [], ...
                                      ENABLE_LONG_LONG, ...
                                      chunk_size);
                fclose(wf);
                c_beautifier(multiwordTypesFile);
            end
            
            if GEN_MDLREF_TYPES && overwriteTimingBridge
                mdlrefTypesFile = fullfile(configInfo.GenDirectory,'model_reference_types.h');
                mf=fopen(mdlrefTypesFile, 'wt');
                dumpMdlrefTypeDefs(mf, configInfo.PurelyIntegerCode);
                fclose(mf);
                c_beautifier(mdlrefTypesFile);
            end
            
            if GEN_BUILTIN_TYPEID_TYPES && overwriteGenBuiltInDTEnums
                builtinTypeidTypesFile = fullfile(configInfo.GenDirectory,'builtin_typeid_types.h');
                tf = fopen(builtinTypeidTypesFile, 'wt');
                dumpBuiltinTypeidTypes(tf, configInfo.PurelyIntegerCode);
                fclose(tf);
                c_beautifier(builtinTypeidTypesFile);
            end
        
            return
        
        end % if strcmpi(style,'full')

        if simulinkInfo.ReplacementTypesOn
            [ReplaceTypesIncludeHdr, ...
             ReplaceRealTypeDefs, ...
             ReplaceImagTypeDefs] = loc_get_replacement_typedefs(...
                 GEN_CMPLX_TYPES, simulinkInfo, ...
                 ~isempty(ansi_int8.val), ~isempty(ansi_uint8.val), ...
                 ~isempty(ansi_int16.val), ~isempty(ansi_uint16.val), ...
                 ~isempty(ansi_int32.val), ~isempty(ansi_uint32.val), ...
                 ~isempty(ansi_real32.val), ~isempty(ansi_real64.val), ...
                 ~isempty(ansi_boolean.val));
        end
        
        hostWordLengths = [];               
        if (ENABLE_PORTABLE_WORDSIZES == 1)
            %Get host word lengths to try to map to target sizes            
            hostWordLengths = getHostWordLengths;
            validatePortableWordSizesMappings(hostWordLengths, hardwareDeploy);
        end
        
        if overwriteCoderTypes
            % collect included header for Simulink Coder replacement data types.
            % create the file in specified directory.
            f=fopen(tmwtypesFile, 'wt');

            % following lines begin creation of original tmwtypes.h
            fprintf(f, '%s\n', '  ');
            fprintf(f, '%s\n', '#ifndef __RTWTYPES_H__  ');
            fprintf(f, '%s\n', '#define __RTWTYPES_H__  ');
            fprintf(f, '%s\n', '  ');
            fprintf(f, '%s\n', '/* Logical type definitions */  ');
            fprintf(f, '%s\n', '#if (!defined(__cplusplus))  ');
            fprintf(f, '%s\n', '#  ifndef false');                                     
            fprintf(f, '%s\n', '#   define false (0U)');                                
            fprintf(f, '%s\n', '#  endif');                                            
            fprintf(f, '%s\n', '#  ifndef true   ');                                   
            fprintf(f, '%s\n', '#   define true (1U)');                                 
            fprintf(f, '%s\n', '#  endif  ');                                          
            fprintf(f, '%s\n', '#endif');                     

            if simulinkInfo.GenErtSFcnRTWTypes
                fprintf(f, '%s\n', '#ifndef __TMWTYPES__  ');
            end
            
            ansi_char       = rtwprivate('getAnsiDataType',ansiDataTypeTable,'tSS_CHAR');
            ansi_short      = rtwprivate('getAnsiDataType',ansiDataTypeTable,'tSS_SHORT');
            ansi_int        = rtwprivate('getAnsiDataType',ansiDataTypeTable,'tSS_INT');
            ansi_long       = rtwprivate('getAnsiDataType',ansiDataTypeTable,'tSS_LONG');
            
            if hardwareImp.ShiftRightIntArith
                shiftRight = 'on';
            else
                shiftRight = 'off';
            end

            fprintf(f, '%s\n', '#define __TMWTYPES__  ');
            fprintf(f, '%s\n', '  ');
            fprintf(f, '%s\n', '  ');
            fprintf(f, '%s\n', '/*=======================================================================*  ');
            fprintf(f, '%s\n', ' * Target hardware information                                           ');
            fprintf(f, '%s\n',[' *   Device type: ' hardwareImp.HWDeviceType]);
            fprintf(f, '%s%3d%s%3d%s%3d\n', ' *   Number of bits:     char: ', ansi_char.numBits, ...
                    '    short:  ', ansi_short.numBits, '    int: ', ansi_int.numBits);
            fprintf(f, '%s%3d',' *                       long: ', ansi_long.numBits);
            
            %print only if long long is enabled
            if (ENABLE_LONG_LONG == 1)
                ansi_long_long  = rtwprivate('getAnsiDataType',ansiDataTypeTable,'tSS_LONG_LONG');
                fprintf(f, '%s%3d','    long long: ', ansi_long_long.numBits);
            end
            fprintf(f, '%s\n', '  ');
            fprintf(f, '%s%3d\n',' *                       native word size: ', ...
                    rtwprivate('getAnsiDataType',ansiDataTypeTable,'NativeWordSize'));
            fprintf(f, '%s\n',[' *   Byte ordering: ', hardwareImp.Endianess]);
            fprintf(f, '%s\n',[' *   Signed integer division rounds to: ', hardwareImp.IntDivRoundTo]);
            fprintf(f, '%s\n',[' *   Shift right on a signed integer as arithmetic shift: ', shiftRight]);
            fprintf(f, '%s\n', ' *=======================================================================*/  ');
            fprintf(f, '%s\n', '  ');
                        
            if (ENABLE_PORTABLE_WORDSIZES == 1)                                              
                fprintf(f, '%s\n', '   ');
                fprintf(f, '%s\n', '#ifdef PORTABLE_WORDSIZES   /* PORTABLE_WORDSIZES defined */');
                fprintf(f, '%s\n', '  ');
                fprintf(f, '%s\n', '/*=======================================================================*  ');
                fprintf(f, '%s\n', ' * Host information                                           ');
                fprintf(f, '%s%3d%s%3d%s%3d\n', ' *   Number of bits:     char: ', hostWordLengths.CharNumBits, ...
                        '    short:  ', hostWordLengths.ShortNumBits, '    int: ', hostWordLengths.IntNumBits);
                fprintf(f, '%s%3d\n',' *                       long: ', hostWordLengths.LongNumBits);
                if(hostWordLengths.LongLongMode == 1)
                    fprintf(f, '%s%3d\n',' *                       long long: ', hostWordLengths.LongLongNumBits);
                end
                fprintf(f, '%s%3d\n',' *                       native word size: ', hostWordLengths.WordSize);
                fprintf(f, '%s\n', ' *=======================================================================*/  ');
                fprintf(f, '%s\n', '  ');
                fprintf(f, '%s\n', '/*=======================================================================*  ');
                fprintf(f, '%s\n', ' * Fixed width word size data types:                                     *  ');
                fprintf(f, '%s\n', ' *   int8_T, int16_T, int32_T     - signed 8, 16, or 32 bit integers     *  ');
                fprintf(f, '%s\n', ' *   uint8_T, uint16_T, uint32_T  - unsigned 8, 16, or 32 bit integers   *  ');
                if ~configInfo.PurelyIntegerCode
                    fprintf(f, '%s\n', ' *   real32_T, real64_T           - 32 and 64 bit floating point numbers *  ');
                end
                if ~ideal_ansi_sizes
                    fprintf(f, '%s\n', ' *                                                                       *  ');
                    fprintf(f, '%s\n', ' *                                                                       *  ');
                    fprintf(f, '%s\n', ' *   Note:  Because the specified hardware does not have native support  *  ');
                    fprintf(f, '%s\n', ' *          for all data sizes, some data types are actually typedef''ed  *  ');
                    fprintf(f, '%s\n', ' *          from larger native data sizes.  The following data types are *  ');
                    fprintf(f, '%s\n', ' *          not in the ideal native data types:                          *  ');
                    fprintf(f, '%s\n', ' *                                                                       *  ');
                    if ~ansi_int8.native_support
                        fprintf(f, '%s\n', ' *          int8_T, uint8_T                                              *  ');
                    end
                    if ~ansi_int16.native_support
                        fprintf(f, '%s\n', ' *          int16_T, uint16_T                                            *  ');
                    end
                    if ~ansi_int32.native_support
                        fprintf(f, '%s\n', ' *          int32_T, uint32_T                                            *  ');
                    end
                end
                fprintf(f, '%s\n', ' *=======================================================================*/  ');
                fprintf(f, '%s\n', '  ');
                
                schar_name = []; %#ok<NASGU>
                uchar_name = []; %#ok<NASGU>
                [uchar_name, schar_name] = locMapToHost(hostWordLengths, hardwareDeploy, 8);
                fprintf(f, '%s\n', ['typedef ' schar_name  ' int8_T;']);
                fprintf(f, '%s\n', ['typedef ' uchar_name  ' uint8_T;']);
                
                [uName, sName] = locMapToHost(hostWordLengths, hardwareDeploy, 16);
                fprintf(f, '%s\n', ['typedef ' sName  ' int16_T;']);
                fprintf(f, '%s\n', ['typedef ' uName  ' uint16_T;']);
                
                sint_name = []; %#ok<NASGU>
                uint_name = []; %#ok<NASGU>
                [uint_name, sint_name] = locMapToHost(hostWordLengths, hardwareDeploy, 32);
                fprintf(f, '%s\n', ['typedef ' sint_name  ' int32_T;']);
                fprintf(f, '%s\n', ['typedef ' uint_name  ' uint32_T;']);
                
                if ~configInfo.PurelyIntegerCode
                    if ~isempty(ansi_real32.val)
                        fprintf(f, '%s\n', ['typedef ' ansi_real32.val ' real32_T;']);
                    end
                    if ~isempty(ansi_real64.val)
                        fprintf(f, '%s\n', ['typedef ' ansi_real64.val ' real64_T;']);
                    end
                end
                
                fprintf(f, '%s\n', '  ');
                fprintf(f, '%s\n', '/*===========================================================================*  ');
                if(hostWordLengths.LongLongMode == 1)
                    if isempty(ansi_real64.val)
                        fprintf(f, '%s\n', ' * Generic type definitions: boolean_T, int_T, uint_T, ulong_T, char_T,      *  ');
                        fprintf(f, '%s\n', ' *                           ulonglong_T and byte_T.                         *  ');
                    else
                        fprintf(f, '%s\n', ' * Generic type definitions: real_T, time_T, boolean_T, int_T, uint_T,       *  ');
                        fprintf(f, '%s\n', ' *                           ulong_T, char_T , ulonglong_T and byte_T.       *  ');
                    end
                else
                    if isempty(ansi_real64.val)
                        fprintf(f, '%s\n', ' * Generic type definitions: boolean_T, int_T, uint_T, ulong_T, char_T,      *  ');
                        fprintf(f, '%s\n', ' *                           and byte_T.                                     *  ');
                    else
                        fprintf(f, '%s\n', ' * Generic type definitions: real_T, time_T, boolean_T, int_T, uint_T,       *  ');
                        fprintf(f, '%s\n', ' *                           ulong_T, char_T and byte_T.                     *  ');
                    end
                end            
                fprintf(f, '%s\n', ' *===========================================================================*/  ');
                fprintf(f, '%s\n', '  ');
                
                
                if ~configInfo.PurelyIntegerCode
                    if ~isempty(ansi_real64.val)
                        fprintf(f, '%s\n', ['typedef ' ansi_real64.val ' real_T;']);
                        fprintf(f, '%s\n', ['typedef ' ansi_real64.val ' time_T;']);
                    elseif ~isempty(ansi_real32.val)
                        fprintf(f, '%s\n', ['typedef ' ansi_real32.val ' real_T;']);
                        fprintf(f, '%s\n', ['typedef ' ansi_real32.val ' time_T;']);
                    end
                end
                
                % This is checking if Data Type Replacement has been used
                % to redefine boolean. uint8 is the default.
                boolean_is_int8 = 0;
                boolean_is_uint16 = 0; 
                boolean_is_int16 = 0;
                boolean_is_uint32 = 0; 
                boolean_is_int32 = 0;
                if simulinkInfo.ReplacementTypesOn
                    if ~isempty(simulinkInfo.ReplacementTypesStruct.boolean)
                        if strcmp(ansi_boolean.val,ansi_int8.val)
                            boolean_is_int8 = 1;
                        elseif strcmp(ansi_boolean.val,ansi_uint16.val)
                            boolean_is_uint16 = 1;
                        elseif strcmp(ansi_boolean.val,ansi_int16.val)
                            boolean_is_int16 = 1;
                        elseif strcmp(ansi_boolean.val,ansi_uint32.val)
                            boolean_is_uint32 = 1;
                        elseif strcmp(ansi_boolean.val,ansi_int32.val)
                            boolean_is_int32 = 1;
                        end
                    end
                end

                % Put out the appropriate typedef for boolean
                if boolean_is_int8
                    if ~isempty(schar_name)
                        fprintf(f, '%s\n', ['typedef ' schar_name  ' boolean_T;']);
                    end
                elseif boolean_is_int16
                    if ~isempty(sName)
                        fprintf(f, '%s\n', ['typedef ' sName  ' boolean_T;']);
                    end
                elseif boolean_is_int32
                    if ~isempty(sint_name)
                        fprintf(f, '%s\n', ['typedef ' sint_name  ' boolean_T;']);
                    end
                elseif boolean_is_uint16
                    if ~isempty(uName)
                        fprintf(f, '%s\n', ['typedef ' uName  ' boolean_T;']);
                    end
                elseif boolean_is_uint32
                    if ~isempty(uint_name)
                        fprintf(f, '%s\n', ['typedef ' uint_name  ' boolean_T;']);
                    end
                else
                    % default: boolean is unsigned char
                    if ~isempty(uchar_name)
                        fprintf(f, '%s\n', ['typedef ' uchar_name  ' boolean_T;']);
                    end
                end
                
                % Put out int_T and uint_T
                if ~isempty(sint_name)
                    fprintf(f, '%s\n', ['typedef ' sint_name  ' int_T;']);
                end
                if ~isempty(uint_name)
                    fprintf(f, '%s\n', ['typedef ' uint_name  ' uint_T;']);
                end

                [uName, ~] = locMapToHost(hostWordLengths, hardwareDeploy, hardwareDeploy.LongNumBits);
                fprintf(f, '%s\n', ['typedef ' uName  ' ulong_T;']);
                
                if(hardwareDeploy.LongLongMode == 1)
                    [uName, ~] = locMapToHost(hostWordLengths, hardwareDeploy, hardwareDeploy.LongLongNumBits);
                    fprintf(f, '%s\n', ['typedef ' uName  ' ulonglong_T;']);
                end
                
                %Can't use target char size due to string manipulations
                % (get this error from rt_logging.c:
                %  error: wchar_t-array initialized from non-wide string)
                fprintf(f, '%s\n', 'typedef char char_T;');
                fprintf(f, '%s\n', 'typedef unsigned char uchar_T;');
                fprintf(f, '%s\n', 'typedef char_T byte_T;  ');
                
                
                if(hardwareDeploy.LongLongMode == 1)
                    deploySizes = [hardwareDeploy.CharNumBits ...
                                   hardwareDeploy.ShortNumBits ...
                                   hardwareDeploy.IntNumBits ...
                                   hardwareDeploy.LongNumBits ...
                                   hardwareDeploy.LongLongNumBits];
                else
                    deploySizes = [hardwareDeploy.CharNumBits ...
                                   hardwareDeploy.ShortNumBits ...
                                   hardwareDeploy.IntNumBits ...
                                   hardwareDeploy.LongNumBits];
                end
                
                [~, IA, ~] = intersect(64, deploySizes);
                if ~isempty(IA)
                    [uName, sName] = locMapToHost(hostWordLengths, hardwareDeploy, 64);
                    fprintf(f, '%s\n', ['typedef ' sName  ' int64_T;']);
                    fprintf(f, '%s\n', ['typedef ' uName ' uint64_T;']);
                end
                
                fprintf(f, '%s\n', '#else  /* PORTABLE_WORDSIZES not defined */ ');
            end
            fprintf(f, '%s\n', '  ');
            fprintf(f, '%s\n', '/*=======================================================================*  ');
            fprintf(f, '%s\n', ' * Fixed width word size data types:                                     *  ');
            fprintf(f, '%s\n', ' *   int8_T, int16_T, int32_T     - signed 8, 16, or 32 bit integers     *  ');
            fprintf(f, '%s\n', ' *   uint8_T, uint16_T, uint32_T  - unsigned 8, 16, or 32 bit integers   *  ');
            if ~configInfo.PurelyIntegerCode
                fprintf(f, '%s\n', ' *   real32_T, real64_T           - 32 and 64 bit floating point numbers *  ');
            end
            if ~ideal_ansi_sizes
                fprintf(f, '%s\n', ' *                                                                       *  ');
                fprintf(f, '%s\n', ' *                                                                       *  ');
                fprintf(f, '%s\n', ' *   Note:  Because the specified hardware does not have native support  *  ');
                fprintf(f, '%s\n', ' *          for all data sizes, some data types are actually typedef''ed  *  ');
                fprintf(f, '%s\n', ' *          from larger native data sizes.  The following data types are *  ');
                fprintf(f, '%s\n', ' *          not in the ideal native data types:                          *  ');
                fprintf(f, '%s\n', ' *                                                                       *  ');
                if ~ansi_int8.native_support
                    fprintf(f, '%s\n', ' *          int8_T, uint8_T                                              *  ');
                end
                if ~ansi_int16.native_support
                    fprintf(f, '%s\n', ' *          int16_T, uint16_T                                            *  ');
                end
                if ~ansi_int32.native_support
                    fprintf(f, '%s\n', ' *          int32_T, uint32_T                                            *  ');
                end
            end
            fprintf(f, '%s\n', ' *=======================================================================*/  ');
            fprintf(f, '%s\n', '  ');

            if ~isempty(ansi_int8.val)
                fprintf(f, '%s\n', ['typedef ' ansi_int8.val   ' int8_T;']);
            end
            if ~isempty(ansi_uint8.val)
                fprintf(f, '%s\n', ['typedef ' ansi_uint8.val  ' uint8_T;']);
            end
            if ~isempty(ansi_int16.val)
                fprintf(f, '%s\n', ['typedef ' ansi_int16.val  ' int16_T;']);
            end
            if ~isempty(ansi_uint16.val)
                fprintf(f, '%s\n', ['typedef ' ansi_uint16.val ' uint16_T;']);
            end
            if ~isempty(ansi_int32.val)
                fprintf(f, '%s\n', ['typedef ' ansi_int32.val  ' int32_T;']);
            end
            if ~isempty(ansi_uint32.val)
                fprintf(f, '%s\n', ['typedef ' ansi_uint32.val ' uint32_T;']);
            end
            if ~isempty(ansi_int64.val)
                fprintf(f, '%s\n', ['typedef ' ansi_int64.val  ' int64_T;']);
            end
            if ~isempty(ansi_uint64.val)
                fprintf(f, '%s\n', ['typedef ' ansi_uint64.val ' uint64_T;']);
            end

            if ~isempty(ansi_real32.val)
                fprintf(f, '%s\n', ['typedef ' ansi_real32.val ' real32_T;']);
            end
            if ~isempty(ansi_real64.val)
                fprintf(f, '%s\n', ['typedef ' ansi_real64.val ' real64_T;']);
                % Note we're going to take care of the replacement type for 'double' below,
                % while taking care of the generic 'real_T' typedef.
            end
            
            fprintf(f, '%s\n', '  ');
            fprintf(f, '%s\n', '/*===========================================================================*  ');
            if(ENABLE_LONG_LONG == 1)
                if isempty(ansi_real64.val)
                    fprintf(f, '%s\n', ' * Generic type definitions: boolean_T, int_T, uint_T, ulong_T, char_T,      *  ');
                    fprintf(f, '%s\n', ' *                           ulonglong_T and byte_T.                         *  ');
                else
                    fprintf(f, '%s\n', ' * Generic type definitions: real_T, time_T, boolean_T, int_T, uint_T,       *  ');
                    fprintf(f, '%s\n', ' *                           ulong_T, char_T , ulonglong_T and byte_T.       *  ');
                end
            else
                if isempty(ansi_real64.val)
                    fprintf(f, '%s\n', ' * Generic type definitions: boolean_T, int_T, uint_T, ulong_T, char_T,      *  ');
                    fprintf(f, '%s\n', ' *                           and byte_T.                                     *  ');
                else
                    fprintf(f, '%s\n', ' * Generic type definitions: real_T, time_T, boolean_T, int_T, uint_T,       *  ');
                    fprintf(f, '%s\n', ' *                           ulong_T, char_T and byte_T.                     *  ');
                end
            end            
            fprintf(f, '%s\n', ' *===========================================================================*/  ');
            fprintf(f, '%s\n', '  ');

            if ~isempty(ansi_real64.val)
                fprintf(f, '%s\n', ['typedef ' ansi_real64.val ' real_T;']);
                fprintf(f, '%s\n', ['typedef ' ansi_real64.val ' time_T;']);
            elseif ~isempty(ansi_real32.val)
                fprintf(f, '%s\n', ['typedef ' ansi_real32.val ' real_T;']);
                fprintf(f, '%s\n', ['typedef ' ansi_real32.val ' time_T;']);
            end

			ansi_boolean.val = '_bit';
            if ~isempty(ansi_boolean.val)
                fprintf(f, '%s\n', ['typedef ' ansi_boolean.val ' boolean_T;']);
            end

            %define int_T & uint_T
            ansi_int = rtwprivate('getAnsiDataType',ansiDataTypeTable,'tSS_INT');
            switch (ansi_int.numBits)
              case 8
                fprintf(f, '%s\n', ['typedef ' ansi_int8.val   ' int_T; ']);
                fprintf(f, '%s\n', ['typedef ' ansi_uint8.val  ' uint_T; ']);
              case 16
                fprintf(f, '%s\n', ['typedef ' ansi_int16.val  ' int_T; ']);
                fprintf(f, '%s\n', ['typedef ' ansi_uint16.val ' uint_T; ']);
              case 32
                fprintf(f, '%s\n', ['typedef ' ansi_int32.val  ' int_T; ']);
                fprintf(f, '%s\n', ['typedef ' ansi_uint32.val ' uint_T; ']);
              otherwise
                fprintf(f, '%s\n', 'typedef int int_T;  ');
                fprintf(f, '%s\n', 'typedef unsigned uint_T;  ');
            end

            fprintf(f, '%s\n', 'typedef unsigned long ulong_T;');
            
            if(ENABLE_LONG_LONG == 1)
                fprintf(f, '%s\n', 'typedef unsigned long long ulonglong_T;');
            end

            %define char_T byte_T
            fprintf(f, '%s\n', 'typedef char char_T;  ');
            fprintf(f, '%s\n', 'typedef unsigned char uchar_T;  ');
            
            
            fprintf(f, '%s\n', 'typedef char_T byte_T;  ');
            fprintf(f, '%s\n', '  ');

            if (ENABLE_PORTABLE_WORDSIZES == 1)
                fprintf(f, '%s\n', '   ');
                fprintf(f, '%s\n', '#endif  /* PORTABLE_WORDSIZES */  ');
                fprintf(f, '%s\n', '   ');
            end
            
            if GEN_CMPLX_TYPES
                dumpComplexTypes(f, mdlName, complexAlignInfo, ansi_support_fp, ...
                                 ansi_real32, ansi_real64, ...
                                 ansi_int8, ansi_int16, ...
                                 ansi_int32, ansi_int64, ...
                                 ansi_uint8, ansi_uint16, ...
                                 ansi_uint32, ansi_uint64);
            end
            
            %dtr = simulinkInfo.ReplacementTypesStruct;
            %[max_i8, min_i8] = get_int_type_limit_ids('int8', dtr, 'MAX_int8_T', 'MIN_int8_T');
            %[max_ui8, min_ui8] = get_int_type_limit_ids('uint8', dtr, 'MAX_uint8_T', 'MIN_uint8_T');
            %[max_i16, min_i16] = get_int_type_limit_ids('int16', dtr, 'MAX_int16_T', 'MIN_int16_T');
            %[max_ui16, min_ui16] = get_int_type_limit_ids('uint16', dtr, 'MAX_uint16_T', 'MIN_uint16_T');
            %[max_i32, min_i32] = get_int_type_limit_ids('int32', dtr, 'MAX_int32_T', 'MIN_int32_T');
            %[max_ui32, min_ui32] = get_int_type_limit_ids('uint32', dtr, 'MAX_uint32_T', 'MIN_uint32_T');
            %[max_i64, min_i64] = get_int_type_limit_ids('int64', dtr, 'MAX_int64_T', 'MIN_int64_T');
            %[max_ui64, min_ui64] = get_int_type_limit_ids('uint64', dtr, 'MAX_uint64_T', 'MIN_uint64_T');
             max_i8 = 'MAX_int8_T'; min_i8 = 'MIN_int8_T';
             max_ui8 = 'MAX_uint8_T'; min_ui8 = 'MIN_uint8_T';
             max_i16 = 'MAX_int16_T'; min_i16 = 'MIN_int16_T';
             max_ui16 = 'MAX_uint16_T'; min_ui16 = 'MIN_uint16_T';
             max_i32 = 'MAX_int32_T'; min_i32 = 'MIN_int32_T';
             max_ui32 = 'MAX_uint32_T'; min_ui32 = 'MIN_uint32_T';
             max_i64 = 'MAX_int64_T'; min_i64 = 'MIN_int64_T';
             max_ui64 = 'MAX_uint64_T'; min_ui64 = 'MIN_uint64_T';
            
            fprintf(f, '%s\n', '  ');
            fprintf(f, '%s\n', '/*=======================================================================*  ');
            fprintf(f, '%s\n', ' * Min and Max:                                                          *  ');
            fprintf(f, '%s\n', ' *   int8_T, int16_T, int32_T     - signed 8, 16, or 32 bit integers     *  ');
            fprintf(f, '%s\n', ' *   uint8_T, uint16_T, uint32_T  - unsigned 8, 16, or 32 bit integers   *  ');
            fprintf(f, '%s\n', ' *=======================================================================*/  ');
            fprintf(f, '%s\n', '  ');
            fprintf(f, '%s\n', sprintf('#define  %s      ((int8_T)(127))              ', max_i8));
            fprintf(f, '%s\n', sprintf('#define  %s      ((int8_T)(-128))             ', min_i8));
            fprintf(f, '%s\n', sprintf('#define  %s     ((uint8_T)(255U))             ', max_ui8));
            fprintf(f, '%s\n', sprintf('#define  %s     ((uint8_T)(0U))  ',              min_ui8));
            fprintf(f, '%s\n', sprintf('#define  %s     ((int16_T)(32767))           ', max_i16));
            fprintf(f, '%s\n', sprintf('#define  %s     ((int16_T)(-32768))          ', min_i16));
            fprintf(f, '%s\n', sprintf('#define  %s    ((uint16_T)(65535U))          ', max_ui16));
            fprintf(f, '%s\n', sprintf('#define  %s    ((uint16_T)(0U))  ',             min_ui16));
            fprintf(f, '%s\n', sprintf('#define  %s     ((int32_T)(2147483647))      ', max_i32));
            fprintf(f, '%s\n', sprintf('#define  %s     ((int32_T)(-2147483647-1))   ', min_i32));
            fprintf(f, '%s\n', sprintf('#define  %s    ((uint32_T)(0xFFFFFFFFU))    ',  max_ui32));
            fprintf(f, '%s\n', sprintf('#define  %s    ((uint32_T)(0U))  ',             min_ui32));
            % 2^63 =  9223372036854775808L
            if (~isempty(ansi_int64.val) && strcmp(ansi_int64.val,'long'))
                fprintf(f, '%s\n', sprintf('#define  %s     ((int64_T)(9223372036854775807L))     ', max_i64));
                fprintf(f, '%s\n', sprintf('#define  %s     ((int64_T)(-9223372036854775807L-1L)) ', min_i64));
            elseif (~isempty(ansi_int64.val) && strcmp(ansi_int64.val,'long long'))
                fprintf(f, '%s\n', sprintf('#define  %s     ((int64_T)(9223372036854775807LL))     ', max_i64));
                fprintf(f, '%s\n', sprintf('#define  %s     ((int64_T)(-9223372036854775807LL-1LL)) ', min_i64));
            end
            
            if (~isempty(ansi_uint64.val) && strcmp(ansi_uint64.val,'unsigned long'))
                fprintf(f, '%s\n', sprintf('#define  %s    ((uint64_T)(0xFFFFFFFFFFFFFFFFUL)) ', max_ui64));
                fprintf(f, '%s\n', sprintf('#define  %s    ((uint64_T)(0UL))  ', min_ui64));
            elseif (~isempty(ansi_uint64.val) && strcmp(ansi_uint64.val,'unsigned long long'))
                fprintf(f, '%s\n', sprintf('#define  %s    ((uint64_T)(0xFFFFFFFFFFFFFFFFULL)) ', max_ui64));
                fprintf(f, '%s\n', sprintf('#define  %s    ((uint64_T)(0ULL))  ', min_ui64));
            end

            fprintf(f, '%s\n', '  ');
            fprintf(f, '%s\n', '  ');
            if simulinkInfo.GenErtSFcnRTWTypes
                fprintf(f, '%s\n', '#endif /* __TMWTYPES__ */  ');
            end
            fprintf(f, '%s\n', '  ');
            fprintf(f, '%s\n', '/* Block D-Work pointer type */');
            fprintf(f, '%s\n', 'typedef void * pointer_T;   ');
            fprintf(f, '%s\n', '  ');

            % emit additional typedef's
            dumpAdditionalTypeDefs(f, ansiDataTypeTable, false, hardwareDeploy, ...
                                   ENABLE_PORTABLE_WORDSIZES, hostWordLengths, ENABLE_LONG_LONG);

            % emit additional complex types
            if GEN_CMPLX_TYPES
                dumpAdditionalComplexTypes(...
                    f, ansiDataTypeTable, true, false, ...
                    ENABLE_LONG_LONG, mdlName, complexAlignInfo, ...
                    ReplaceImagTypeDefs);
            end

            if simulinkInfo.ReplacementTypesOn && ... 
                    (~isempty(ReplaceTypesIncludeHdr) || ...
                    ~isempty(ReplaceRealTypeDefs))
                fprintf(f, '%s\n', '  ');
                fprintf(f, '%s\n', '/* Define Simulink Coder replacement data types. */  ');
                fprintf(f, ReplaceTypesIncludeHdr);
                fprintf(f, '%s\n', '  ');
                fprintf(f, ReplaceRealTypeDefs);
                fprintf(f, '%s\n', '  ');
            end
                
            if usingSimulink
                fprintf(f, '%s\n', '/* Simulink specific types */  ');
                fprintf(f, '%s\n', '  ');
                simstrc_types_buffer = rtwprivate('gen_simstruc_types', ...
                                                  configInfo.PurelyIntegerCode, ...
                                                  simulinkInfo);
                fprintf(f, '%s\n', simstrc_types_buffer);
            end
            
            fprintf(f, '%s\n', '#endif /* __RTWTYPES_H__ */  ');
            
            fclose(f);
            c_beautifier(tmwtypesFile);
        
        end
        
        if simulinkInfo.GenErtSFcnRTWTypes
            return
        end
        
        if GEN_MULTIWORD_TYPES && overwriteMultiwordTypes
            multiwordTypesFile = fullfile(configInfo.GenDirectory,'multiword_types.h');
            wf=fopen(multiwordTypesFile, 'wt');
            dumpMultiWordTypeDefs(wf, ...
                                  simulinkInfo.GenChunkDefs, ...                                                               
                                  'chunks', ...
                                  GEN_CMPLX_TYPES, ...
                                  MaxMultiwordBits, ...
                                  hardwareDeploy, ...
                                  ENABLE_PORTABLE_WORDSIZES, ...
                                  hostWordLengths, ...
                                  ENABLE_LONG_LONG, ...
                                  chunk_size);
            fclose(wf);
            c_beautifier(multiwordTypesFile);
        end

        if GEN_MDLREF_TYPES && overwriteTimingBridge
            mdlrefTypesFile = fullfile(configInfo.GenDirectory,'model_reference_types.h');
            mf=fopen(mdlrefTypesFile, 'wt');
            dumpMdlrefTypeDefs(mf, configInfo.PurelyIntegerCode);
            fclose(mf);
            c_beautifier(mdlrefTypesFile);
        end

        if GEN_BUILTIN_TYPEID_TYPES && overwriteGenBuiltInDTEnums
            builtinTypeidTypesFile = fullfile(configInfo.GenDirectory,'builtin_typeid_types.h');
            tf = fopen(builtinTypeidTypesFile, 'wt');
            dumpBuiltinTypeidTypes(tf, configInfo.PurelyIntegerCode);
            fclose(tf);
            c_beautifier(builtinTypeidTypesFile);
        end
        
    catch last_error_msg_id
        ansiDataTypeName = false;  %#ok<NASGU>
        if f ~= -1
            fclose(f); % make sure close handle
        end
        if mf ~= -1
            fclose(mf); % make sure close handle
        end
        if tf ~= -1
            fclose(tf); % make sure close handle
        end
        if wf ~= -1
            fclose(wf); % make sure close handle
        end
        rethrow(last_error_msg_id);
    end

    return

% Function: get_type_limit_ids()
%   This function will return the maximum and minimum type limit 
%   identifeirs for the given size specific integers type using the given 
%   DTR information. If there is no replacement type for the indicated 
%   type, the given default values are used.
%   Inputs:
%       inputType     - Type for which the limit identifiers are searched.
%       repTypeStruct - Data Type Replacement (DTR) information.
%       defaultMaxId  - Max ID default used if no DTR is found.
%       defaultMinId  - Min ID default used if no DTR is found.
%   Outputs:
%       maxId         - Replacement or default MAX ID.
%       minId         - Replacement or default MIN ID.
%   
function [maxId, minId] = get_int_type_limit_ids(inputType, repTypesStruct, ...
    defaultMaxId, defaultMinId)
    if isempty(repTypesStruct)
        repType = [];
    else
        repType = repTypesStruct.(inputType);
    end
    if isempty(repType) || ~evalin('base', ['exist(''' repType ''',''var'')'])
        maxId = defaultMaxId;
        minId = defaultMinId;
    else
        repAliasType = evalin('base', repType);
        % If replacement type name is an existing variable, then it should
        % be an AliasType.
        if isa(repAliasType, 'Simulink.AliasType')
            maxId = repAliasType.TypeMaxId;
            minId = repAliasType.TypeMinId;
            % Limit identifier fields could be empty.
            if isempty(maxId)
                maxId = defaultMaxId;
            end
            if isempty(minId)
                minId = defaultMinId;
            end
        else
            % E.g., base workspace object is NumericType.
            maxId = defaultMaxId;
            minId = defaultMinId;
        end
    end

%==========================================================================
% Function: getHostWordLengths()
%   Returns the host word lengths
function hostWordLengths = getHostWordLengths

hostWordLengths = rtwhostwordlengths();
hostWordLengths.CharNumBits     = int32(hostWordLengths.CharNumBits);
hostWordLengths.ShortNumBits    = int32(hostWordLengths.ShortNumBits);
hostWordLengths.IntNumBits      = int32(hostWordLengths.IntNumBits);
hostWordLengths.LongNumBits     = int32(hostWordLengths.LongNumBits);
hostWordLengths.LongLongNumBits = int32(hostWordLengths.LongLongNumBits);
hostWordLengths.WordSize        = int32(hostWordLengths.LongNumBits);

%==========================================================================
% Function: validatePortableWordSizesMappings(hostWordLengths,
% hardwareDeploy)
%
%    Checks that there exists a mapping from production hardware data types
%    to host data types.
%==========================================================================
function validatePortableWordSizesMappings(hostWordLengths, hardwareDeploy)

% determine if there is a mapping for the production int
prodIntMapping =  (hostWordLengths.LongNumBits == hardwareDeploy.IntNumBits) || ...
                  (hostWordLengths.IntNumBits == hardwareDeploy.IntNumBits) || ...
                  (hostWordLengths.ShortNumBits == hardwareDeploy.IntNumBits) || ...
                  (hostWordLengths.CharNumBits == hardwareDeploy.IntNumBits);
        
% determine if there is a mapping for the production long                
prodLongMapping = (hostWordLengths.LongNumBits == hardwareDeploy.LongNumBits) || ...
                    (hostWordLengths.IntNumBits == hardwareDeploy.LongNumBits);                                
if ~prodLongMapping && hostWordLengths.LongLongMode    
    % check for mapping to host long long
    prodLongMapping = hostWordLengths.LongLongNumBits == hardwareDeploy.LongNumBits;
    if prodLongMapping
        % There is a mapping from prod long to host long long but this is
        % not supported because the generated code may contain long 
        % constants with "L" suffix.   These may become corrupt when 
        % applied to long long without "LL" suffix (g992895)
        DAStudio.error('RTW:buildProcess:InvalidProdLongToHostLongLongMapping', int2str(hardwareDeploy.LongNumBits));        
    end
end
    
% determine if there is a mapping for the production long long
if hardwareDeploy.LongLongMode           
    prodLongLongMapping = hostWordLengths.LongNumBits == hardwareDeploy.LongLongNumBits;
    if ~prodLongLongMapping && hostWordLengths.LongLongMode
        prodLongLongMapping = hostWordLengths.LongLongNumBits == hardwareDeploy.LongLongNumBits;
    end    
else           
    % production long long is not applicable
    prodLongLongMapping = true;    
end

if ~prodLongLongMapping || ~prodLongMapping || ~prodIntMapping
    DAStudio.error('RTW:buildProcess:missingTargetIntOrLongDataTypeForPortableWordSizes');
end


%==========================================================================
% Function: loc_define_replacementtype()
%   This takes care of building up the typedefs to support the 
%   Data Type Replacement dialog. This is called for each replacement type, 
%   to append its typedef to the string being built up. 
%   'baseName' is the base type, e.g. 'int32_T' or 'boolean_T' or ...
%   AliasData is the alias name, i.e. what appeared in the
%   Replacement Type dialog. So for example if baseName is 'int8_T'
%   and AliasData is 's1', we'll end up putting out
%   typedef int8_T s1;
%   The 's' in/out argument is the string of typedefs we have built up so far,
%   which we use to avoid putting out duplicate typedefs.
%   
function [realTypedefs, imagTypedefs, ReplaceTypesHdrList] = ...
    loc_define_replacementtype(...
        realTypedefs, imagTypedefs, ...
        ReplaceTypesHdrList, AliasData, baseName, support_complex, ...
        designDataLocation)
    if ~isempty(AliasData)

        [~,aHeader] = coder.internal.findBaseType(designDataLocation,AliasData); 


        % See if AliasData specifies a header file
        if ~isempty(aHeader)
            foundMatch = false;
            for i=1:length(ReplaceTypesHdrList)
                if strcmp(aHeader, ReplaceTypesHdrList{i}.HeaderFile)
                    ReplaceTypesHdrList{i}.DataTypes{end+1} = baseName;
                    foundMatch = true;
                    break;
                end
            end
            if ~foundMatch
                ReplaceTypesHdrList{end+1}.HeaderFile = aHeader;
                ReplaceTypesHdrList{end}.DataTypes{1} = baseName;
            end
        else

            % If we get here, 'AliasData' is a name to be typedef'ed to type 'baseName'.
            % We want to avoid putting out duplicate typedefs. 
            % Checking for this is based on seeing if the typedef we are
            % about to put out is a substring of what we've already put out.
            % We have to be a bit careful about how we go about this.
            % For example, we want to put out only 1 of these 2 redundant typedefs:
            %   typedef int32_T s1; /* User defined ...  
            %   typedef int_T s1; /* User defined ...  
            % However, we want to put out both of the following and not be fooled 
            % by the fact that "s1" is a substring of "s1s1". 
            %   typedef int16_T s1; /* User defined replacement datatype for ...  
            %   typedef int32_T s1s1; /* User defined replacement datatype for ...  
            % Therefore we are careful to include the space before the alias name
            % in the duplicate check string, but not include the base type name.
            dupChkStr = [' ' AliasData '; /* User defined replacement datatype for '];
            if isempty(strfind(realTypedefs, dupChkStr))  
                realTypedefs = [realTypedefs sprintf('%s\n', ['  typedef ' baseName ' ' AliasData '; /* User defined replacement datatype for ' baseName ' */'])];
                % For compatibility with user-defined types, it is necessary
                % to also put out the complex typename. For example, if
                % x1 is a Simulink.AliasType object whose base type is 'int32', 
                % then you can use x1 or cx1 directly in the model,
                % and these need to be typedef'ed to int32_T and cint32_T respectively.
                % However, only do this for the base types that actually *have*
                % complex typenames associated with them.
                if support_complex && ...
                  ((strcmp(baseName, 'real32_T')) || (strcmp(baseName, 'real64_T')) || ...
                   (strcmp(baseName, 'real_T')) || ...
                   (strcmp(baseName, 'int8_T')) || (strcmp(baseName, 'uint8_T')) || ...
                   (strcmp(baseName, 'int16_T')) || (strcmp(baseName,'uint16_T')) || ...
                   (strcmp(baseName, 'int32_T')) || (strcmp(baseName, 'uint32_T')))
                    cAliasData = ['c' AliasData];
                    cBaseName = ['c' baseName];
                    imagTypedefs = [imagTypedefs sprintf('%s\n', ['  typedef ' cBaseName ' ' cAliasData '; /* User defined replacement datatype for ' cBaseName ' */'])];
                end
            end
        end
    end

function [ReplaceTypesIncludeHdr, ...
          ReplaceRealTypeDefs, ...
          ReplaceImagTypeDefs] = loc_get_replacement_typedefs(...
              GEN_CMPLX_TYPES, ...
              simulinkInfo, ...
              int8Defined, ...
              uint8Defined, ...
              int16Defined, ...
              uint16Defined, ...
              int32Defined, ...
              uint32Defined, ...
              real32Defined, ...
              real64Defined, ...
              booleanDefined)

    ReplaceTypesHdrList = {};
    ReplaceTypesIncludeHdr = '';
    ReplaceRealTypeDefs = '';        
    ReplaceImagTypeDefs = '';        
    if int8Defined
        [ReplaceRealTypeDefs, ReplaceImagTypeDefs, ReplaceTypesHdrList] = loc_define_replacementtype(ReplaceRealTypeDefs, ReplaceImagTypeDefs, ReplaceTypesHdrList, simulinkInfo.ReplacementTypesStruct.int8, 'int8_T', GEN_CMPLX_TYPES, simulinkInfo.DesignDataLocation);
    end
    if uint8Defined
        [ReplaceRealTypeDefs, ReplaceImagTypeDefs, ReplaceTypesHdrList] = loc_define_replacementtype(ReplaceRealTypeDefs, ReplaceImagTypeDefs, ReplaceTypesHdrList, simulinkInfo.ReplacementTypesStruct.uint8, 'uint8_T', GEN_CMPLX_TYPES, simulinkInfo.DesignDataLocation);
    end
    if int16Defined
        [ReplaceRealTypeDefs, ReplaceImagTypeDefs, ReplaceTypesHdrList] = loc_define_replacementtype(ReplaceRealTypeDefs, ReplaceImagTypeDefs, ReplaceTypesHdrList, simulinkInfo.ReplacementTypesStruct.int16, 'int16_T', GEN_CMPLX_TYPES, simulinkInfo.DesignDataLocation);
    end
    if uint16Defined
        [ReplaceRealTypeDefs, ReplaceImagTypeDefs, ReplaceTypesHdrList] = loc_define_replacementtype(ReplaceRealTypeDefs, ReplaceImagTypeDefs, ReplaceTypesHdrList, simulinkInfo.ReplacementTypesStruct.uint16, 'uint16_T', GEN_CMPLX_TYPES, simulinkInfo.DesignDataLocation);
    end
    if int32Defined
        [ReplaceRealTypeDefs, ReplaceImagTypeDefs, ReplaceTypesHdrList] = loc_define_replacementtype(ReplaceRealTypeDefs, ReplaceImagTypeDefs, ReplaceTypesHdrList, simulinkInfo.ReplacementTypesStruct.int32, 'int32_T', GEN_CMPLX_TYPES, simulinkInfo.DesignDataLocation);
    end
    if uint32Defined
        [ReplaceRealTypeDefs, ReplaceImagTypeDefs, ReplaceTypesHdrList] = loc_define_replacementtype(ReplaceRealTypeDefs, ReplaceImagTypeDefs, ReplaceTypesHdrList, simulinkInfo.ReplacementTypesStruct.uint32, 'uint32_T', GEN_CMPLX_TYPES, simulinkInfo.DesignDataLocation);
    end
    if real32Defined
        [ReplaceRealTypeDefs, ReplaceImagTypeDefs, ReplaceTypesHdrList] = loc_define_replacementtype(ReplaceRealTypeDefs, ReplaceImagTypeDefs, ReplaceTypesHdrList, simulinkInfo.ReplacementTypesStruct.single, 'real32_T', GEN_CMPLX_TYPES, simulinkInfo.DesignDataLocation);
    end
    if real64Defined
        [ReplaceRealTypeDefs, ReplaceImagTypeDefs, ReplaceTypesHdrList] = loc_define_replacementtype(ReplaceRealTypeDefs, ReplaceImagTypeDefs, ReplaceTypesHdrList, simulinkInfo.ReplacementTypesStruct.double, 'real_T', GEN_CMPLX_TYPES, simulinkInfo.DesignDataLocation);
        % The following is a fallback for the case where a replacement type
        % for 'double' was defined, but there is no ansi_real64 type.
        % In that case we fall back to typedefing the 'double' replacement type
        % to 'real_T' which is typedef'ed to the ansi_real32 type.
    elseif real32Defined
        [ReplaceRealTypeDefs, ReplaceImagTypeDefs, ReplaceTypesHdrList] = loc_define_replacementtype(ReplaceRealTypeDefs, ReplaceImagTypeDefs, ReplaceTypesHdrList, simulinkInfo.ReplacementTypesStruct.double, 'real_T', GEN_CMPLX_TYPES, simulinkInfo.DesignDataLocation);
    end
    if booleanDefined
        [ReplaceRealTypeDefs, ReplaceImagTypeDefs, ReplaceTypesHdrList] = loc_define_replacementtype(ReplaceRealTypeDefs, ReplaceImagTypeDefs, ReplaceTypesHdrList, simulinkInfo.ReplacementTypesStruct.boolean, 'boolean_T', GEN_CMPLX_TYPES, simulinkInfo.DesignDataLocation);
    end        
    [ReplaceRealTypeDefs, ReplaceImagTypeDefs, ReplaceTypesHdrList] = loc_define_replacementtype(ReplaceRealTypeDefs, ReplaceImagTypeDefs, ReplaceTypesHdrList, simulinkInfo.ReplacementTypesStruct.int, 'int_T', GEN_CMPLX_TYPES, simulinkInfo.DesignDataLocation);
    [ReplaceRealTypeDefs, ReplaceImagTypeDefs, ReplaceTypesHdrList] = loc_define_replacementtype(ReplaceRealTypeDefs, ReplaceImagTypeDefs, ReplaceTypesHdrList, simulinkInfo.ReplacementTypesStruct.uint, 'uint_T', GEN_CMPLX_TYPES, simulinkInfo.DesignDataLocation);
    [ReplaceRealTypeDefs, ReplaceImagTypeDefs, ReplaceTypesHdrList] = loc_define_replacementtype(ReplaceRealTypeDefs, ReplaceImagTypeDefs, ReplaceTypesHdrList, simulinkInfo.ReplacementTypesStruct.char, 'char_T', GEN_CMPLX_TYPES, simulinkInfo.DesignDataLocation);
    for i=1:length(ReplaceTypesHdrList)
        DataTypeList='';
        for j=1:length(ReplaceTypesHdrList{i}.DataTypes)
            DataTypeList = [DataTypeList ReplaceTypesHdrList{i}.DataTypes{j} ' ']; %#ok
        end
        %check delimiters
        rheader = ReplaceTypesHdrList{i}.HeaderFile;
        if (rheader(1)=='"' && rheader(end)=='"') ||...
                (rheader(1)=='<' && rheader(end)=='>')
            ReplaceTypesIncludeHdr = [ReplaceTypesIncludeHdr sprintf('%s\n', ['  #include ' ReplaceTypesHdrList{i}.HeaderFile ' /* User defined replacement datatype for ' DataTypeList ' */ '])]; %#ok<AGROW>
        else
            ReplaceTypesIncludeHdr = [ReplaceTypesIncludeHdr sprintf('%s\n', ['  #include "' ReplaceTypesHdrList{i}.HeaderFile '" /* User defined replacement datatype for ' DataTypeList ' */ '])]; %#ok<AGROW>
        end
    end
    
function dumpAdditionalTypeDefs(f, ansiDataTypeTable, ...
    needMicroProtectionFor64Bits, hardwareDeploy, portWordSizes, hostWordLengths,ENABLE_LONG_LONG)

    if (needMicroProtectionFor64Bits) 
        ansi_int64         = rtwprivate('getAnsiDataType',ansiDataTypeTable,'tSS_INT64');
        ansi_uint64        = rtwprivate('getAnsiDataType',ansiDataTypeTable,'tSS_UINT64');
        ansi_int64.numBits  = 64;
        ansi_uint64.numBits = 64;
        ansi_int64.typeName  = 'int64_T';
        ansi_uint64.typeName = 'uint64_T';

        locUtilDumpTypeDef(f, ansi_int64, needMicroProtectionFor64Bits);
        locUtilDumpTypeDef(f, ansi_uint64, needMicroProtectionFor64Bits);
    end

    ansi_char_deploy          = rtwprivate('getAnsiDataType',ansiDataTypeTable,'tSS_CHAR_DEPLOY');
    ansi_uchar_deploy         = rtwprivate('getAnsiDataType',ansiDataTypeTable,'tSS_UCHAR_DEPLOY');
    ansi_short_deploy         = rtwprivate('getAnsiDataType',ansiDataTypeTable,'tSS_SHORT_DEPLOY');
    ansi_ushort_deploy        = rtwprivate('getAnsiDataType',ansiDataTypeTable,'tSS_USHORT_DEPLOY');
    ansi_int_deploy           = rtwprivate('getAnsiDataType',ansiDataTypeTable,'tSS_INT_DEPLOY');
    ansi_uint_deploy          = rtwprivate('getAnsiDataType',ansiDataTypeTable,'tSS_UINT_DEPLOY');
    ansi_long_deploy          = rtwprivate('getAnsiDataType',ansiDataTypeTable,'tSS_LONG_DEPLOY');
    ansi_ulong_deploy         = rtwprivate('getAnsiDataType',ansiDataTypeTable,'tSS_ULONG_DEPLOY');
   
    if(ENABLE_LONG_LONG == 1)
        ansi_longlong_deploy  = rtwprivate('getAnsiDataType',ansiDataTypeTable,'tSS_LONG_LONG_DEPLOY');
        ansi_ulonglong_deploy = rtwprivate('getAnsiDataType',ansiDataTypeTable,'tSS_ULONG_LONG_DEPLOY');
    end
    
    needAdditionalHardwareDefinitions = false;
    if ~isempty(ansi_char_deploy.val) || ...
            ~isempty(ansi_short_deploy.val) || ...
            ~isempty(ansi_int_deploy.val) || ...
            ~isempty(ansi_long_deploy.val) || ...
            ((ENABLE_LONG_LONG == 1) && ~isempty(ansi_longlong_deploy.val))
        fprintf(f, '%s\n', '  ');
        fprintf(f, '%s\n', '/*===========================================================================*  ');
        fprintf(f, '%s\n', ' * Additional type definitions: Embedded Hardware                            *  ');
        fprintf(f, '%s\n', ' *===========================================================================*/  ');
        fprintf(f, '%s\n', '  ');
        needAdditionalHardwareDefinitions = true;
        if (portWordSizes == 1)
            fprintf(f, '%s\n', '   ');
            fprintf(f, '%s\n', '#ifdef PORTABLE_WORDSIZES   /* PORTABLE_WORDSIZES defined */');
            fprintf(f, '%s\n', '  ');
            if ~isempty(ansi_char_deploy.val)
                numBits = hardwareDeploy.CharNumBits;
                uTypeName = ['uint', num2str(numBits), '_T'];
                typeName  = ['int',  num2str(numBits), '_T'];
                [uchar_name, schar_name] = locMapToHost(hostWordLengths, hardwareDeploy, numBits);
                fprintf(f, '%s\n', ['typedef ', schar_name, ' ', typeName, ';']);
                fprintf(f, '%s\n', ['typedef ', uchar_name, ' ', uTypeName, ';']);
            end
            if ~isempty(ansi_short_deploy.val)
                numBits = hardwareDeploy.ShortNumBits;
                uTypeName = ['uint', num2str(numBits), '_T'];
                typeName  = ['int',  num2str(numBits), '_T'];
                [uchar_name, schar_name] = locMapToHost(hostWordLengths, hardwareDeploy, numBits);
                fprintf(f, '%s\n', ['typedef ', schar_name, ' ', typeName, ';']);
                fprintf(f, '%s\n', ['typedef ', uchar_name, ' ', uTypeName, ';']);
            end
            if ~isempty(ansi_int_deploy.val)
                numBits = hardwareDeploy.IntNumBits;
                uTypeName = ['uint', num2str(numBits), '_T'];
                typeName  = ['int',  num2str(numBits), '_T'];
                [uchar_name, schar_name] = locMapToHost(hostWordLengths, hardwareDeploy, numBits);
                fprintf(f, '%s\n', ['typedef ', schar_name, ' ', typeName, ';']);
                fprintf(f, '%s\n', ['typedef ', uchar_name, ' ', uTypeName, ';']);
            end
            if ~isempty(ansi_long_deploy.val)
                numBits = hardwareDeploy.LongNumBits;
                uTypeName = ['uint', num2str(numBits), '_T'];
                typeName  = ['int',  num2str(numBits), '_T'];
                [uchar_name, schar_name] = locMapToHost(hostWordLengths, hardwareDeploy, numBits);
                fprintf(f, '%s\n', ['typedef ', schar_name, ' ', typeName, ';']);
                fprintf(f, '%s\n', ['typedef ', uchar_name, ' ', uTypeName, ';']);
            end
            if ((ENABLE_LONG_LONG == 1) && ~isempty(ansi_longlong_deploy.val))
                numBits = hardwareDeploy.LongLongNumBits;
                uTypeName = ['uint', num2str(numBits), '_T'];
                typeName  = ['int',  num2str(numBits), '_T'];
                [uchar_name, schar_name] = locMapToHost(hostWordLengths, hardwareDeploy, numBits);
                fprintf(f, '%s\n', ['typedef ', schar_name, ' ', typeName, ';']);
                fprintf(f, '%s\n', ['typedef ', uchar_name, ' ', uTypeName, ';']);
            end
            fprintf(f, '%s\n', '   ');
            fprintf(f, '%s\n', '#else  /* PORTABLE_WORDSIZES not defined */ ');
            fprintf(f, '%s\n', '   ');
        end
    end
    
    locUtilDumpTypeDef(f, ansi_char_deploy, needMicroProtectionFor64Bits);
    locUtilDumpTypeDef(f, ansi_uchar_deploy, needMicroProtectionFor64Bits);
    locUtilDumpTypeDef(f, ansi_short_deploy, needMicroProtectionFor64Bits);
    locUtilDumpTypeDef(f, ansi_ushort_deploy, needMicroProtectionFor64Bits);
    locUtilDumpTypeDef(f, ansi_int_deploy, needMicroProtectionFor64Bits);
    locUtilDumpTypeDef(f, ansi_uint_deploy, needMicroProtectionFor64Bits);
    locUtilDumpTypeDef(f, ansi_long_deploy, needMicroProtectionFor64Bits);
    locUtilDumpTypeDef(f, ansi_ulong_deploy, needMicroProtectionFor64Bits);
    
    if (ENABLE_LONG_LONG == 1)
        locUtilDumpTypeDef(f, ansi_longlong_deploy, needMicroProtectionFor64Bits);
        locUtilDumpTypeDef(f, ansi_ulonglong_deploy, needMicroProtectionFor64Bits);
    end

    ansi_char                 = rtwprivate('getAnsiDataType',ansiDataTypeTable,'tSS_CHAR');
    ansi_uchar                = rtwprivate('getAnsiDataType',ansiDataTypeTable,'tSS_UCHAR');
    ansi_short                = rtwprivate('getAnsiDataType',ansiDataTypeTable,'tSS_SHORT');
    ansi_ushort               = rtwprivate('getAnsiDataType',ansiDataTypeTable,'tSS_USHORT');
    ansi_int                  = rtwprivate('getAnsiDataType',ansiDataTypeTable,'tSS_INT');
    ansi_uint                 = rtwprivate('getAnsiDataType',ansiDataTypeTable,'tSS_UINT');
    ansi_long                 = rtwprivate('getAnsiDataType',ansiDataTypeTable,'tSS_LONG');
    ansi_ulong                = rtwprivate('getAnsiDataType',ansiDataTypeTable,'tSS_ULONG');
    
    if (ENABLE_LONG_LONG == 1)
        ansi_longlong         = rtwprivate('getAnsiDataType',ansiDataTypeTable,'tSS_LONG_LONG');
        ansi_ulonglong        = rtwprivate('getAnsiDataType',ansiDataTypeTable,'tSS_ULONG_LONG');
    end

    if ~isempty(ansi_char.val) || ...
            ~isempty(ansi_short.val) || ...
            ~isempty(ansi_int.val) || ...
            ~isempty(ansi_long.val)
        fprintf(f, '%s\n', '  ');
        fprintf(f, '%s\n', '/*===========================================================================*  ');
        fprintf(f, '%s\n', ' * Additional type definitions: Emulation Hardware                           *  ');
        fprintf(f, '%s\n', ' *===========================================================================*/  ');
        fprintf(f, '%s\n', '  ');
    end

    locUtilDumpTypeDef(f, ansi_char, needMicroProtectionFor64Bits);
    locUtilDumpTypeDef(f, ansi_uchar, needMicroProtectionFor64Bits);
    locUtilDumpTypeDef(f, ansi_short, needMicroProtectionFor64Bits);
    locUtilDumpTypeDef(f, ansi_ushort, needMicroProtectionFor64Bits);
    locUtilDumpTypeDef(f, ansi_int, needMicroProtectionFor64Bits);
    locUtilDumpTypeDef(f, ansi_uint, needMicroProtectionFor64Bits);
    locUtilDumpTypeDef(f, ansi_long, needMicroProtectionFor64Bits);
    locUtilDumpTypeDef(f, ansi_ulong, needMicroProtectionFor64Bits);
    
    if(ENABLE_LONG_LONG == 1)
        locUtilDumpTypeDef(f, ansi_longlong, needMicroProtectionFor64Bits);
        locUtilDumpTypeDef(f, ansi_ulonglong, needMicroProtectionFor64Bits);
    end
    
    if (portWordSizes == 1 && needAdditionalHardwareDefinitions)
        fprintf(f, '%s\n', '   ');
        fprintf(f, '%s\n', '#endif  /* PORTABLE_WORDSIZES */  ');
        fprintf(f, '%s\n', '   ');
    end

%endfunction

function dumpComplexTypes(f, mdlName, complexAlignInfo, ansi_support_fp, ...
                          ansi_real32, ansi_real64, ...
                          ansi_int8, ansi_int16, ansi_int32, ansi_int64, ...
                          ansi_uint8, ansi_uint16, ansi_uint32, ansi_uint64)
    fprintf(f, '%s\n', '/*===========================================================================*  ');
    fprintf(f, '%s\n', ' * Complex number type definitions                                           *  ');
    fprintf(f, '%s\n', ' *===========================================================================*/  ');

    % define floating point reals
    if (ansi_support_fp)
        fprintf(f, '%s\n', '#define CREAL_T');
        fprintf(f, '%s\n', '  ');
        %define creal32_T
        if ~isempty(ansi_real32.val)
            printfComplexTypeDef(f, mdlName, 'creal32_T', 'real32_T', ...
                                 complexAlignInfo, 'single');
        end            %REAL32_T_defined
        
        %define creal64_T
        if ~isempty(ansi_real64.val)
            printfComplexTypeDef(f, mdlName, 'creal64_T', ...
                                 'real64_T', complexAlignInfo, 'double');
        end            %REAL64_T_defined
        
        %define creal_T
        if (~isempty(ansi_real64.val) || ~isempty(ansi_real32.val))
            printfComplexTypeDef(f, mdlName, 'creal_T', ...
                                 'real_T', complexAlignInfo, 'double');
        end            %REAL64_T_defined
    end % floating point reals
    
    %define cint8_T
    if ~isempty(ansi_int8.val)
        fprintf(f, '%s\n', '#define CINT8_T');
        fprintf(f, '%s\n', '  ');                
        printfComplexTypeDef(f, mdlName, 'cint8_T', ...
                             'int8_T', complexAlignInfo, 'int8');
    end
    
    %define cuint8_T
    if ~isempty(ansi_uint8.val)
        fprintf(f, '%s\n', '#define CUINT8_T');
        fprintf(f, '%s\n', '  ');                
        printfComplexTypeDef(f, mdlName, 'cuint8_T', ...
                             'uint8_T', complexAlignInfo, 'uint8');
    end
    
    %define cint16_T
    if ~isempty(ansi_int16.val)
        fprintf(f, '%s\n', '#define CINT16_T');
        fprintf(f, '%s\n', '  ');                
        printfComplexTypeDef(f, mdlName, 'cint16_T', ...
                             'int16_T', complexAlignInfo, 'int16');
    end
    
    %define cuint16_T
    if ~isempty(ansi_uint16.val)
        fprintf(f, '%s\n', '#define CUINT16_T');
        fprintf(f, '%s\n', '  ');                
        printfComplexTypeDef(f, mdlName, 'cuint16_T', ...
                             'uint16_T', complexAlignInfo, 'uint16');
    end
    
    %define cint32_T
    if ~isempty(ansi_int32.val)
        fprintf(f, '%s\n', '#define CINT32_T');
        fprintf(f, '%s\n', '  ');                
        printfComplexTypeDef(f, mdlName, 'cint32_T', ...
                             'int32_T', complexAlignInfo, 'int32');
    end
    
    %define cuint32_T
    if ~isempty(ansi_uint32.val)
        fprintf(f, '%s\n', '#define CUINT32_T');
        fprintf(f, '%s\n', '  ');                
        printfComplexTypeDef(f, mdlName, 'cuint32_T', ...
                             'uint32_T', complexAlignInfo, 'uint32');
    end
    
    %define cint64_T
    if ~isempty(ansi_int64.val)
        fprintf(f, '%s\n', '#define CINT64_T');
        fprintf(f, '%s\n', '  ');                
        printfComplexTypeDef(f, mdlName, 'cint64_T', ...
                             'int64_T', complexAlignInfo, 'int64');
    end
    
    %define cuint64_T
    if ~isempty(ansi_uint64.val)
        fprintf(f, '%s\n', '#define CUINT64_T');
        fprintf(f, '%s\n', '  ');                
        printfComplexTypeDef(f, mdlName, 'cuint64_T', ...
                             'uint64_T', complexAlignInfo, 'uint64');
    end
    
%endfunction

function dumpAdditionalComplexTypes(f, ansiDataTypeTable, showComments, ...
                                    needMicroProtectionFor64Bits, ...
                                    ENABLE_LONG_LONG, mdlName, ...
                                    complexAlignInfo, ReplaceImagTypeDefs)

    ansi_char_deploy          = rtwprivate('getAnsiDataType',ansiDataTypeTable,'tSS_CHAR_DEPLOY');
    ansi_uchar_deploy         = rtwprivate('getAnsiDataType',ansiDataTypeTable,'tSS_UCHAR_DEPLOY');
    ansi_short_deploy         = rtwprivate('getAnsiDataType',ansiDataTypeTable,'tSS_SHORT_DEPLOY');
    ansi_ushort_deploy        = rtwprivate('getAnsiDataType',ansiDataTypeTable,'tSS_USHORT_DEPLOY');
    ansi_int_deploy           = rtwprivate('getAnsiDataType',ansiDataTypeTable,'tSS_INT_DEPLOY');
    ansi_uint_deploy          = rtwprivate('getAnsiDataType',ansiDataTypeTable,'tSS_UINT_DEPLOY');
    ansi_long_deploy          = rtwprivate('getAnsiDataType',ansiDataTypeTable,'tSS_LONG_DEPLOY');
    ansi_ulong_deploy         = rtwprivate('getAnsiDataType',ansiDataTypeTable,'tSS_ULONG_DEPLOY');
    ansi_char                 = rtwprivate('getAnsiDataType',ansiDataTypeTable,'tSS_CHAR');
    ansi_uchar                = rtwprivate('getAnsiDataType',ansiDataTypeTable,'tSS_UCHAR');
    ansi_short                = rtwprivate('getAnsiDataType',ansiDataTypeTable,'tSS_SHORT');
    ansi_ushort               = rtwprivate('getAnsiDataType',ansiDataTypeTable,'tSS_USHORT');
    ansi_int                  = rtwprivate('getAnsiDataType',ansiDataTypeTable,'tSS_INT');
    ansi_uint                 = rtwprivate('getAnsiDataType',ansiDataTypeTable,'tSS_UINT');
    ansi_long                 = rtwprivate('getAnsiDataType',ansiDataTypeTable,'tSS_LONG');
    ansi_ulong                = rtwprivate('getAnsiDataType',ansiDataTypeTable,'tSS_ULONG');
    
    if(ENABLE_LONG_LONG == 1)
        ansi_longlong_deploy  = rtwprivate('getAnsiDataType',ansiDataTypeTable,'tSS_LONG_LONG_DEPLOY');
        ansi_ulonglong_deploy = rtwprivate('getAnsiDataType',ansiDataTypeTable,'tSS_ULONG_LONG_DEPLOY');
        ansi_longlong         = rtwprivate('getAnsiDataType',ansiDataTypeTable,'tSS_LONG_LONG');
        ansi_ulonglong        = rtwprivate('getAnsiDataType',ansiDataTypeTable,'tSS_ULONG_LONG');
    end
    
    moreCondition = false;
    
    if needMicroProtectionFor64Bits
        ansi_int64         = rtwprivate('getAnsiDataType',ansiDataTypeTable,'tSS_INT64');
        ansi_uint64        = rtwprivate('getAnsiDataType',ansiDataTypeTable,'tSS_UINT64');
        ansi_int64.numBits   = 64;
        ansi_uint64.numBits  = 64;
        ansi_int64.typeName  = 'int64_T';
        ansi_uint64.typeName = 'uint64_T';
        
        moreCondition = ~isempty(ansi_int64.val) || ~isempty(ansi_uint64.val);
    end
    
    if(ENABLE_LONG_LONG == 1)
        moreCondition = moreCondition || ~isempty(ansi_longlong.val) || ~isempty(ansi_ulonglong.val) || ...
            ~isempty(ansi_longlong_deploy.val) || ~isempty(ansi_ulonglong_deploy.val);
    end
    
    if showComments
        if ~isempty(ansi_uchar_deploy.val) || ...
                ~isempty(ansi_char_deploy.val) || ...
                ~isempty(ansi_uchar.val) || ...
                ~isempty(ansi_char.val) || ...
                ~isempty(ansi_ushort_deploy.val) || ...
                ~isempty(ansi_short_deploy.val) || ...
                ~isempty(ansi_ushort.val) || ...
                ~isempty(ansi_short.val) || ...
                ~isempty(ansi_uint_deploy.val) || ...
                ~isempty(ansi_int_deploy.val) || ...
                ~isempty(ansi_uint.val) || ...
                ~isempty(ansi_int.val) || ...
                ~isempty(ansi_ulong_deploy.val) || ...
                ~isempty(ansi_long_deploy.val) || ...
                ~isempty(ansi_ulong.val) || ...
                ~isempty(ansi_long.val) || ...
                moreCondition
            
            
            fprintf(f, '\n');
            fprintf(f, '%s\n', '/*===========================================================================*  ');
            fprintf(f, '%s\n', ' * Additional complex number type definitions                                           *  ');
            fprintf(f, '%s\n', ' *===========================================================================*/  ');
        end
    end
    
    if needMicroProtectionFor64Bits
        locUtilDumpComplexTypeDef(f, ansi_int64, needMicroProtectionFor64Bits, mdlName,complexAlignInfo,false);
        locUtilDumpComplexTypeDef(f, ansi_uint64, needMicroProtectionFor64Bits, mdlName,complexAlignInfo,false);
    end
    
    locUtilDumpComplexTypeDef(f, ansi_char_deploy, needMicroProtectionFor64Bits, mdlName,complexAlignInfo,false);
    locUtilDumpComplexTypeDef(f, ansi_uchar_deploy, needMicroProtectionFor64Bits, mdlName,complexAlignInfo,false);
    locUtilDumpComplexTypeDef(f, ansi_short_deploy, needMicroProtectionFor64Bits, mdlName,complexAlignInfo,false);
    locUtilDumpComplexTypeDef(f, ansi_ushort_deploy, needMicroProtectionFor64Bits, mdlName,complexAlignInfo,false);
    locUtilDumpComplexTypeDef(f, ansi_int_deploy, needMicroProtectionFor64Bits, mdlName,complexAlignInfo,true);
    locUtilDumpComplexTypeDef(f, ansi_uint_deploy, needMicroProtectionFor64Bits, mdlName,complexAlignInfo,true);
    locUtilDumpComplexTypeDef(f, ansi_long_deploy, needMicroProtectionFor64Bits, mdlName,complexAlignInfo,true);
    locUtilDumpComplexTypeDef(f, ansi_ulong_deploy, needMicroProtectionFor64Bits, mdlName,complexAlignInfo,true);
    
    
    locUtilDumpComplexTypeDef(f, ansi_char, needMicroProtectionFor64Bits, mdlName,complexAlignInfo,false);
    locUtilDumpComplexTypeDef(f, ansi_uchar, needMicroProtectionFor64Bits, mdlName,complexAlignInfo,false);
    locUtilDumpComplexTypeDef(f, ansi_short, needMicroProtectionFor64Bits, mdlName,complexAlignInfo,false);
    locUtilDumpComplexTypeDef(f, ansi_ushort, needMicroProtectionFor64Bits, mdlName,complexAlignInfo,false);
    locUtilDumpComplexTypeDef(f, ansi_int, needMicroProtectionFor64Bits, mdlName,complexAlignInfo,true);
    locUtilDumpComplexTypeDef(f, ansi_uint, needMicroProtectionFor64Bits, mdlName,complexAlignInfo,true);
    locUtilDumpComplexTypeDef(f, ansi_long, needMicroProtectionFor64Bits, mdlName,complexAlignInfo,true);
    locUtilDumpComplexTypeDef(f, ansi_ulong, needMicroProtectionFor64Bits, mdlName,complexAlignInfo,true);
    
    if(ENABLE_LONG_LONG == 1)
        locUtilDumpComplexTypeDef(f, ansi_longlong_deploy, needMicroProtectionFor64Bits, mdlName,complexAlignInfo,true);
        locUtilDumpComplexTypeDef(f, ansi_ulonglong_deploy, needMicroProtectionFor64Bits, mdlName,complexAlignInfo,true);
        locUtilDumpComplexTypeDef(f, ansi_longlong, needMicroProtectionFor64Bits, mdlName,complexAlignInfo,true);
        locUtilDumpComplexTypeDef(f, ansi_ulonglong, needMicroProtectionFor64Bits, mdlName,complexAlignInfo,true);
    end

    if ~isempty(ReplaceImagTypeDefs)
        fprintf(f, '%s\n', '  ');
        fprintf(f, '%s\n', '/* Define Simulink Coder replacement data types. */  ');
        fprintf(f, ReplaceImagTypeDefs);
        fprintf(f, '%s\n', '  ');
    end
%endfunction


function printfComplexTypeDef(f, modelName, complexType, fieldType, complexAlignInfo, mapEntry)
    align = complexAlignInfo.(mapEntry);
    fDAF = complexAlignInfo.fieldDAF;
    sDAF = complexAlignInfo.structDAF;
    
    if ~isempty(modelName)
        daSyntax = locGetComplexTypeAlignmentSyntax(fieldType, align, modelName, fDAF, sDAF);
    else
        daSyntax = {'', ''};
    end
    
    if isempty(daSyntax{1}) && isempty(daSyntax{2})  % plain complex definition
        fprintf(f, '%s\n', '   typedef struct {  ');
        fprintf(f, '%s\n', ['     ', fieldType, ' re;  ']);
        fprintf(f, '%s\n', ['     ', fieldType, ' im;  ']);
        fprintf(f, '%s\n', ['   } ', complexType, ';  ']);
        fprintf(f, '%s\n', '  ');
    elseif fDAF.supported
        fprintf(f, '%s\n', '   typedef struct {  ');
        fprintf(f, '%s\n', ['     ', daSyntax{1}, ' ', fieldType, ' re;  ']);
        fprintf(f, '%s\n', ['     ', daSyntax{2}, ' ', fieldType, ' im;  ']);
        fprintf(f, '%s\n', ['   } ', complexType, ';  ']);
        fprintf(f, '%s\n', '  ');
    elseif sDAF.supported
        if strcmp(sDAF.position, 'DATA_ALIGNMENT_PRECEEDING_STATEMENT')
            fprintf(f, '%s\n', daSyntax{1});
            fprintf(f, '%s\n', '   typedef struct {  ');
        elseif strcmp(sDAF.position, 'DATA_ALIGNMENT_PREDIRECTIVE')
            fprintf(f, '%s\n', ['   typedef struct ', daSyntax{1}, '{  ']);
        else
            fprintf(f, '%s\n', '   typedef struct {  ');
        end                    
        fprintf(f, '%s\n', ['     ', fieldType, ' re;  ']);
        fprintf(f, '%s\n', ['     ', fieldType, ' im;  ']);
        if strcmp(sDAF.position, 'DATA_ALIGNMENT_FOLLOWING_STATEMENT')
            fprintf(f, '%s\n', ['   } ', complexType, ';  ']);
            fprintf(f, '%s\n', daSyntax{1});
        elseif strcmp(sDAF.position, 'DATA_ALIGNMENT_POSTDIRECTIVE')
            fprintf(f, '%s\n', ['   } ', complexType, ' ', daSyntax{1}, ';  ']);            
        else
            fprintf(f, '%s\n', ['   } ', complexType, ';  ']);            
        end                            
        fprintf(f, '%s\n', '  ');        
    end
%endfunction    

    
function locUtilDumpTypeDef(f, ansi_type, needMicroProtectionFor64Bits)

    addMicro = needMicroProtectionFor64Bits && (ansi_type.numBits == 64);

    if ~isempty(ansi_type.val)
        if addMicro
            if strncmpi(ansi_type.val, 'unsigned', size('unsigned',2))
                fprintf(f, '%s\n', '#ifndef UINT64_T');
                fprintf(f, '%s\n', '#define UINT64_T');
            else
                fprintf(f, '%s\n', '#ifndef INT64_T');
                fprintf(f, '%s\n', '#define INT64_T');
            end
        end
        fprintf(f, '%s\n', ['typedef ', ansi_type.val, ' ', ansi_type.typeName, ';']);
        if addMicro
            fprintf(f, '%s\n', '#endif');
        end
    end

function locUtilDumpComplexTypeDef(f, ansi_type, needMicroProtectionFor64Bits ...
                                  , mdlName, complexAlignInfo, queryComplexAlign)

    if ~isempty(ansi_type.val)
        addMicro = needMicroProtectionFor64Bits && (ansi_type.numBits == 64);
        isUnsigned = strncmpi(ansi_type.val, 'unsigned', size('unsigned',2));
        
        if addMicro
            if isUnsigned
                fprintf(f, '%s\n', '#ifndef CUINT64_T');
                fprintf(f, '%s\n', '#define CUINT64_T');
            else
                fprintf(f, '%s\n', '#ifndef CINT64_T');
                fprintf(f, '%s\n', '#define CINT64_T');
            end
        end
        if ~isempty(ansi_type.val)
            if(queryComplexAlign)
              mapEntry = sprintf('int%d',ansi_type.numBits);
              if(isUnsigned)
                mapEntry = ['u',mapEntry];
              end
            else
              mapEntry = 'UnalignedType';
            end
            printfComplexTypeDef(f, mdlName, ['c',ansi_type.typeName], ansi_type.typeName ...
                                , complexAlignInfo, mapEntry);
        end
        if addMicro
            fprintf(f, '%s\n', '#endif');
        end
    end

function dumpBuiltinTypeidTypes(f, NO_FLOATS)
    fprintf(f, '%s\n', '#ifndef __BUILTIN_TYPEID_TYPES_H__  ');
    fprintf(f, '%s\n', '  #define __BUILTIN_TYPEID_TYPES_H__  ');
    fprintf(f, '%s\n', '');
    fprintf(f, '%s\n', '#include "rtwtypes.h"');
    fprintf(f, '%s\n', '');
    fprintf(f, '%s\n', '#ifndef __BUILTIN_TYPEID_TYPES__  ');
    fprintf(f, '%s\n', '  #define __BUILTIN_TYPEID_TYPES__  ');
    fprintf(f, '%s\n', '');
    fprintf(f, '%s\n', '/* Enumeration of built-in data types */ ');
    fprintf(f, '%s\n', 'typedef enum { ');
    if NO_FLOATS
        fprintf(f, '%s\n', '   SS_DOUBLE  =  0,     ');
        fprintf(f, '%s\n', '   SS_SINGLE  =  1,     ');
    else
        fprintf(f, '%s\n', '    SS_DOUBLE  =  0,    /* real_T    */ ');
        fprintf(f, '%s\n', '    SS_SINGLE  =  1,    /* real32_T  */ ');
    end
    fprintf(f, '%s\n', '    SS_INT8    =  2,    /* int8_T    */ ');
    fprintf(f, '%s\n', '    SS_UINT8   =  3,    /* uint8_T   */ ');
    fprintf(f, '%s\n', '    SS_INT16   =  4,    /* int16_T   */ ');
    fprintf(f, '%s\n', '    SS_UINT16  =  5,    /* uint16_T  */ ');
    fprintf(f, '%s\n', '    SS_INT32   =  6,    /* int32_T   */ ');
    fprintf(f, '%s\n', '    SS_UINT32  =  7,    /* uint32_T  */ ');
    fprintf(f, '%s\n', '    SS_BOOLEAN =  8     /* boolean_T */ ');
    fprintf(f, '%s\n', '} BuiltInDTypeId; ');
    fprintf(f, '%s\n', ' ');
    fprintf(f, '%s\n', '#define SS_NUM_BUILT_IN_DTYPE ((int_T)SS_BOOLEAN+1) ');
    fprintf(f, '%s\n', ' ');
    fprintf(f, '%s\n', ' ');
    fprintf(f, '%s\n', '/* Enumeration for MAT-file logging code */ ');
    fprintf(f, '%s\n', 'typedef int_T DTypeId; ');
    fprintf(f, '%s\n', ' ');
    fprintf(f, '%s\n', '#endif /* __BUILTIN_TYPEID_TYPES__ */  ');
    fprintf(f, '%s\n', '#endif /* __BUILTIN_TYPEID_TYPES_H__ */  ');

function dumpMdlrefTypeDefs(f, NO_FLOATS)
    fprintf(f, '%s\n', '#ifndef __MODEL_REFERENCE_TYPES_H__  ');
    fprintf(f, '%s\n', '  #define __MODEL_REFERENCE_TYPES_H__  ');
    fprintf(f, '%s\n', '  #include "rtwtypes.h"  ');
    fprintf(f, '%s\n', '');
    fprintf(f, '%s\n', '#ifndef __MODEL_REFERENCE_TYPES__  ');
    fprintf(f, '%s\n', '  #define __MODEL_REFERENCE_TYPES__  ');
    fprintf(f, '%s\n', '');
    fprintf(f, '%s\n', '/*===========================================================================*  ');
    fprintf(f, '%s\n', ' * Model reference type definitions                                          *  ');
    fprintf(f, '%s\n', ' *===========================================================================*/  ');

    fprintf(f, '%s\n', '/* ');
    fprintf(f, '%s\n', ' * This structure is used by model reference to  ');
    fprintf(f, '%s\n', ' * communicate timing information through the hierarchy. ');
    fprintf(f, '%s\n', ' */ ');
    fprintf(f, '%s\n', 'typedef struct _rtTimingBridge_tag rtTimingBridge; ');
    fprintf(f, '%s\n', ' ');
    fprintf(f, '%s\n', 'struct _rtTimingBridge_tag { ');
    fprintf(f, '%s\n', ' ');
    fprintf(f, '%s\n', '    uint32_T     nTasks; ');
    fprintf(f, '%s\n', ' ');
    fprintf(f, '%s\n', '    uint32_T**   clockTick; ');
    fprintf(f, '%s\n', '    uint32_T**   clockTickH; ');
    fprintf(f, '%s\n', ' ');
    fprintf(f, '%s\n', '    uint32_T*    taskCounter; ');
    fprintf(f, '%s\n', ' ');
    if ~(NO_FLOATS)        
        fprintf(f, '    real_T**     taskTime; ');
    end
    fprintf(f, '%s\n', ' ');
    fprintf(f, '%s\n', '    boolean_T**  rateTransition; ');
    fprintf(f, '%s\n', '     ');
    fprintf(f, '%s\n', '    boolean_T    *firstInitCond; ');
    fprintf(f, '%s\n', '}; ');
    fprintf(f, '%s\n', ' ');
    fprintf(f, '%s\n', '#endif /* __MODEL_REFERENCE_TYPES__ */  ');
    fprintf(f, '%s\n', '#endif /* __MODEL_REFERENCE_TYPES_H__ */  ');

function dumpMultiWordTypeDefs(f, ...
                               genChunkDefs, ...
                               chunksFieldName, ...
                               GEN_CMPLX_TYPES, ...
                               MAX_MULTIWORD_BITS, ...
                               hardwareDeploy, ...
                               portWordSizes, ...
                               hostWordLengths, ...
                               ENABLE_LONG_LONG, ...
                               chunk_size)
%DUMPMULTIWORDTYPEDEFS
%
% tbpll: target bits per long long
% tbpl: target bits per long
% tbpi: target bits per int
%
    
    fprintf(f, '%s\n', '#ifndef __MULTIWORD_TYPES_H__  ');
    fprintf(f, '%s\n', '  #define __MULTIWORD_TYPES_H__  ');
    fprintf(f, '%s\n', '');
    fprintf(f, '%s\n', '#include "rtwtypes.h"');
    fprintf(f, '%s\n', '');
    % Write out supporting definitions for portable word size
    if genChunkDefs
        fprintf(f, '\n/*\n * Definitions supporting external data access \n */\n');
        
        fprintf(f, 'typedef int%s_T chunk_T;\n', num2str(chunk_size));
        fprintf(f, 'typedef uint%s_T uchunk_T;\n', num2str(chunk_size));
    end
    
    if (MAX_MULTIWORD_BITS > chunk_size)

        % Write out supporting definitions for multiword types
        fprintf(f, '\n/*\n * MultiWord supporting definitions\n */\n');
        
        if portWordSizes
            fprintf(f, '%s\n', '   ');
            fprintf(f, '%s\n', '#ifdef PORTABLE_WORDSIZES   /* PORTABLE_WORDSIZES defined */');
            numBits = hardwareDeploy.LongNumBits;
            if (ENABLE_LONG_LONG == 1)
                numBits = hardwareDeploy.LongLongNumBits;
            end
            [~, s_name] = locMapToHost(hostWordLengths, hardwareDeploy, numBits);
            
            if (ENABLE_LONG_LONG == 1)
                fprintf(f, '%s\n', ['typedef ', s_name, ' longlong_T;']);
            else
                fprintf(f, '%s\n', ['typedef ', s_name, ' long_T;']);
            end
            fprintf(f, '%s\n', '   ');
            fprintf(f, '%s\n', '#else  /* PORTABLE_WORDSIZES not defined */ ');
            fprintf(f, '%s\n', '   ');
        end
        
        if (ENABLE_LONG_LONG == 1)
            fprintf(f, 'typedef long long longlong_T;\n');
        else
            fprintf(f, 'typedef long int long_T;\n');
        end
        
        if portWordSizes
            fprintf(f, '%s\n', '   ');
            fprintf(f, '%s\n', '#endif  /* PORTABLE_WORDSIZES */  ');
            fprintf(f, '%s\n', '   ');
        end
        
        % Write out the types
        % Ensure we cover at least the specified lengths. 
        fprintf(f, '\n/*\n * MultiWord types\n */\n');
        signednessPrefix = {'', 'u'}; % 'u' for unsigned
        for nBits = (2*chunk_size : chunk_size : (MAX_MULTIWORD_BITS + chunk_size-1))
            % Write out types for all multiple of chunks,
            % starting with 2 chunks and up to 2048 bits.
            nChunks = nBits / chunk_size;
            for sIndex = 1:2
                % Write out a type for each signedness mode
                
                typeName = [signednessPrefix{sIndex} 'int' num2str(nBits) 'm_T'];
                cTypeName = ['c' typeName];
                
                fprintf(f, '\n');
                
                % Real
                fprintf(f, '\ntypedef struct {\n');
                
                fprintf(f, '  uint%d_T %s[%d];\n', chunk_size, chunksFieldName, nChunks);
                
                fprintf(f, '} %s;\n\n', typeName);
                
                % Complex
                if (GEN_CMPLX_TYPES == 1)
                    fprintf(f, 'typedef struct {\n');
                    fprintf(f, '  %s re;\n', typeName);
                    fprintf(f, '  %s im;\n', typeName);
                    fprintf(f, '} %s;\n\n', cTypeName);
                end
                
            end
        end
    end
    fprintf(f, '%s\n', ' ');
    fprintf(f, '%s\n', '#endif /* __MULTIWORD_TYPES_H__ */  ');

%endfunction

%------------------------------------------------------------------------------
%
% function: locCheckArgs 
%
% inputs:
%
%------------------------------------------------------------------------------
function locCheckArgs(hardwareImp, hardwareDeploy, configInfo, simulinkInfo)

    % Ensure arguments are appropriate
    expectedImp = {'CharNumBits', 'ShortNumBits', 'IntNumBits', 'LongNumBits', 'LongLongNumBits', ...
                   'LongLongMode', 'WordSize', 'ShiftRightIntArith', 'IntDivRoundTo', 'Endianess',...
                   'FloatNumBits', 'DoubleNumBits', 'PointerNumBits', 'HWDeviceType'};
    optionalImp = {'TypeEmulationWarnSuppressLevel', 'PreprocMaxBitsSint',...
                   'PreprocMaxBitsUint'};
    impDiff = setdiff(fieldnames(hardwareImp),{expectedImp{:},optionalImp{:}}); %#ok<CCAT>
    if ~isempty(impDiff)
        if ~isempty(setdiff(impDiff,optionalImp))
            str = [sprintf('%s ',expectedImp{:}), '[ ',...
                           sprintf('%s ',optionalImp{:}),']'];
            DAStudio.error('RTW:buildProcess:invalidStructureArgument',...
                           'hardwareImp',str);
        end
    end

    expectedDeploy = {'CharNumBits', 'ShortNumBits', 'IntNumBits', 'LongNumBits', 'LongLongNumBits',...
                      'LongLongMode', 'FloatNumBits', 'DoubleNumBits', 'PointerNumBits'};
    if ~isempty(setdiff(fieldnames(hardwareDeploy),expectedDeploy))
        str = sprintf('%s ',expectedDeploy{:});
        DAStudio.error('RTW:buildProcess:invalidStructureArgument',...
                       'hardwareDeploy',str);
    end

    expectedConfig = {'GenDirectory','PurelyIntegerCode','SupportComplex',...
                      'MaxMultiwordBits','ModelName'}; 
    if ~isempty(setdiff(fieldnames(configInfo),expectedConfig))
        str = sprintf('%s ',expectedConfig{:});
        DAStudio.error('RTW:buildProcess:invalidStructureArgument',...
                       'configInfo',str);
    end

    expectedSimulink = {'Style','SharedLocation','IsERT','PortableWordSizes',...
                        'SupportNonInlinedSFcns','GRTInterface','ReplacementTypesOn',...
                        'ReplacementTypesStruct', 'DesignDataLocation', 'GenChunkDefs',...
                        'GenBuiltInDTEnums', 'GenTimingBridge','GenErtSFcnRTWTypes'};
    if ~isempty(setdiff(fieldnames(simulinkInfo),expectedSimulink))
        str = sprintf('%s ',expectedSimulink{:});
        DAStudio.error('RTW:buildProcess:invalidStructureArgument',...
                       'simulinkInfo',str);
    end
    
%------------------------------------------------------------------------------
%
% function: locMapToHost
%
% inputs:
%
%------------------------------------------------------------------------------
function [typeNameU, typeNameS] = locMapToHost(hostSizes, hardwareDeploy, numBits)
    typeNameU = []; %#ok<NASGU>
    typeNameS = []; %#ok<NASGU>
    if numBits <= hardwareDeploy.CharNumBits
        [typeNameU, typeNameS] = locGetHostEquivalent(hostSizes, ...
            hardwareDeploy.CharNumBits);
        
    elseif numBits <= hardwareDeploy.ShortNumBits
        [typeNameU, typeNameS] = locGetHostEquivalent(hostSizes, ...
            hardwareDeploy.ShortNumBits);
        
    elseif  numBits <= hardwareDeploy.IntNumBits
        [typeNameU, typeNameS] = locGetHostEquivalent(hostSizes, ...
            hardwareDeploy.IntNumBits);
        
    elseif  numBits <= hardwareDeploy.LongNumBits
        [typeNameU, typeNameS] = locGetHostEquivalent(hostSizes, ...
            hardwareDeploy.LongNumBits);
        
    elseif  (numBits <= hardwareDeploy.LongLongNumBits && ...
             hardwareDeploy.LongLongMode == 1)
        [typeNameU, typeNameS] = locGetHostEquivalent(hostSizes, ...
            hardwareDeploy.LongLongNumBits);
              
    else
        DAStudio.error('RTW:buildProcess:missingHostDataTypeForPortableWordSizes',...
                   numBits);
        
    end
    
%------------------------------------------------------------------------------
%
% function: locGetHostEquivalent
%
% inputs:
%
%------------------------------------------------------------------------------
function [typeNameU, typeNameS] = locGetHostEquivalent(hostSizes, numBits)
    typeNameU = []; %#ok<NASGU>
    typeNameS = []; %#ok<NASGU>
    if numBits <= hostSizes.CharNumBits
        typeNameU = 'unsigned char';
        typeNameS  = 'signed char';
        
    elseif numBits <= hostSizes.ShortNumBits
        typeNameU = 'unsigned short';
        typeNameS  = 'short';
        
    elseif  numBits <= hostSizes.IntNumBits
        typeNameU = 'unsigned int';
        typeNameS  = 'int';
        
    elseif  numBits <= hostSizes.LongNumBits
        typeNameU = 'unsigned long';
        typeNameS  = 'long'; 
        
    elseif  numBits <= hostSizes.LongLongNumBits
        typeNameU = 'unsigned long long';
        typeNameS  = 'long long'; 
        
    else
        DAStudio.error('RTW:buildProcess:missingHostDataTypeForPortableWordSizes',...
                   numBits);
        
    end

    
function syntax = locGetComplexTypeAlignmentSyntax(typeName, align, modelName, fDAF, sDAF)
    syntax = {'',''};
    if align > 0
        if fDAF.supported
            das = rtwprivate('da_syntax', modelName, 're', typeName, ...
                              align, 'DATA_ALIGNMENT_STRUCT_FIELD');
            syntax{1} = das.syntax;
            das = rtwprivate('da_syntax', modelName, 'im', typeName, ...
                              align, 'DATA_ALIGNMENT_STRUCT_FIELD');
            syntax{2} = das.syntax;
        elseif sDAF.supported
            das = rtwprivate('da_syntax', modelName, '', typeName, ...
                              align, 'DATA_ALIGNMENT_WHOLE_STRUCT');
            syntax{1} = das.syntax;
        end
    end
    
function alignInfo = locGetComplextypeAlignmentInfo(mdlName, hardwareImp, hardwareDeploy)
    alignInfo = struct('int8',  -1, 'uint8',  -1, 'int16',  -1, 'uint16', -1, ...
                       'int32', -1, 'uint32', -1, 'int64',  -1, 'uint64', -1, ...
                       'single', -1, 'double', -1);
                     
    % Add unsized types
    unsizedWL = [ hardwareImp.IntNumBits ...
                , hardwareImp.LongNumBits ...
                , hardwareImp.LongLongNumBits ...
                , hardwareDeploy.IntNumBits ...
                , hardwareDeploy.LongNumBits ...
                , hardwareDeploy.LongLongNumBits ...
                ];
    unsizedWL = unique(unsizedWL);
    for idx = 1:numel(unsizedWL)
      unsizedType = sprintf('int%d',unsizedWL(idx));
      alignInfo.(unsizedType) = -1;
      unsizedType = ['u',unsizedType]; %#ok<AGROW>
      alignInfo.(unsizedType) = -1;
    end
    
    if ~isempty(mdlName)
      % Get user specified alignment boundaries
      types = fields(alignInfo);
      hc = get_param(mdlName, 'TargetFcnLibHandle');
      for i=1 : length(types)
          t = types{i};
          alignInfo.(t) = hc.getComplexTypeAlignment(t);
      end
    end
    
    % Add key for any type that shouldn't be aligned
    alignInfo.UnalignedType = -1;
    

    

% LocalWords:  Preproc sharedutils Defs Ansi typedefs rtwtypeschksum typedef's
% LocalWords:  WORDSIZES ulong wchar uchar ansi typedefing typedef'ed reals
% LocalWords:  CREAL creal im cint cuint FFFFFFFFU FFFFFFFFFFFFFFFFUL cplusplus
% LocalWords:  SCHAR replacementtype cx typenames USHORT DUMPMULTIWORDTYPEDEFS
% LocalWords:  tbpl tbpi uchunk
