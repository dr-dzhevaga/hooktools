package ru.nami.hooktools;

import org.apache.commons.lang3.StringUtils;
import org.kohsuke.args4j.Option;
import ru.nami.hooktools.mappping.MappingItem;
import ru.nami.hooktools.mappping.MappingReaderFactory;
import ru.nami.hooktools.mappping.MappingUtil;
import ru.nami.hooktools.util.MainHelper;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * Created by Dmitriy Dzhevaga on 19.01.2016.
 */
public class MappingComparator {
    @Option(name = "-om", aliases = "--origin-mapping", usage = "specify origin mapping file", required = true)
    private String originMappingFile;

    @Option(name = "-tm", aliases = "--target-mapping", usage = "specify target mapping file", required = true)
    private String targetMappingFile;

    @Option(name = "-nm", aliases = "--name-mapping", usage = "specify symbols name mapping file", required = false)
    private String nameMappingFile;

    public static void main(String... args) {
        new MappingComparator().doMain(args);
    }

    public void doMain(String... args) {
        MainHelper.execute(args, this, () -> {
            final List<MappingItem> originMapping = MappingReaderFactory.newReader(originMappingFile).read();
            if (StringUtils.isNotEmpty(nameMappingFile)) {
                MappingUtil.addSymbolsFromNameMappingFile(originMapping, nameMappingFile);
            }

            final List<MappingItem> targetMapping = MappingReaderFactory.newReader(targetMappingFile).read();
            if (StringUtils.isNotEmpty(nameMappingFile)) {
                MappingUtil.addSymbolsFromNameMappingFile(targetMapping, nameMappingFile);
            }

            targetMapping.forEach(targetSymbol -> {
                Optional<MappingItem> originSymbolOpt = MappingUtil.findSymbolByName(originMapping, targetSymbol.getName());
                if (originSymbolOpt.isPresent()) {
                    MappingItem originSymbol = originSymbolOpt.get();
                    if (!Objects.equals(originSymbol.getAddress(), targetSymbol.getAddress()) || !Objects.equals(originSymbol.getBitNumber(), targetSymbol.getBitNumber())) {
                        System.out.println(originSymbol + " / " + targetSymbol);
                    }
                } else {
                    System.out.println("new symbol: " + targetSymbol);
                }
            });
        });
    }
}
