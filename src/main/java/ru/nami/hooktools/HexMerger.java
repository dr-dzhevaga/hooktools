package ru.nami.hooktools;

import org.kohsuke.args4j.Option;
import ru.nami.hooktools.hex.IntelHexReader;
import ru.nami.hooktools.hex.IntelHexWriter;
import ru.nami.hooktools.util.MainHelper;
import ru.nami.hooktools.util.MemoryMerger;
import ru.nami.hooktools.util.MemoryMerger.MergeMode;

import java.util.SortedMap;

/**
 * Created by Dmitriy Dzhevaga on 20.11.2015.
 */
public class HexMerger {
    @Option(name = "-oh", aliases = "--origin-hex", usage = "specify origin hex file", required = true)
    private String originHexFile;

    @Option(name = "-ph", aliases = "--patch-hex", usage = "specify patch hex file", required = true)
    private String patchHexFile;

    @Option(name = "-o", aliases = "--overlap-strategy", usage = "specify patching overlap strategy: keep origin value, override with patch value or throw error", required = false)
    private String overlapStrategy = MergeMode.THROW.toString();

    @Option(name = "-sm", aliases = "--silent-mode", usage = "disable overlap logging in case of 0x00 origin value", required = false)
    private boolean silentMode;

    public static void main(String... args) {
        new HexMerger().doMain(args);
    }

    public void doMain(String... args) {
        MainHelper.execute(args, this, () -> {
            final SortedMap<Long, Integer> originMemory = IntelHexReader.read(originHexFile);
            final SortedMap<Long, Integer> patchMemory = IntelHexReader.read(patchHexFile);
            final MergeMode mergeMode = MergeMode.of(overlapStrategy);
            MemoryMerger.merge(originMemory, patchMemory, mergeMode, silentMode);
            IntelHexWriter.write(originHexFile, originMemory);
        });
    }
}
