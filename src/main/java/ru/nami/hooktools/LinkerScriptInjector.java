package ru.nami.hooktools;

import org.apache.commons.lang3.StringUtils;
import org.kohsuke.args4j.Option;
import ru.nami.hooktools.linker.LinkerScriptWriterFactory;
import ru.nami.hooktools.mappping.MappingItem;
import ru.nami.hooktools.mappping.MappingReaderFactory;
import ru.nami.hooktools.mappping.MappingUtil;
import ru.nami.hooktools.util.MainHelper;

import java.util.List;

/**
 * Created by Dmitriy Dzhevaga on 17.11.2015.
 */
public class LinkerScriptInjector {
    @Option(name = "-om", aliases = "--origin-mapping", usage = "specify origin mapping file", required = true)
    private String mappingFile;

    @Option(name = "-lst", aliases = "--linker-script-template", usage = "specify linker script template file", required = true)
    private String linkerScriptFile;

    @Option(name = "-is", aliases = "--import-symbols", usage = "regular expression for imported symbols names", required = true)
    private String importSymbols;

    @Option(name = "-nm", aliases = "--name-mapping", usage = "symbols name mapping", required = false)
    private String nameMappingFile;

    @Option(name = "-rss", aliases = "--reuse-space-symbols", usage = "regular expression for symbols names to exclude from reserved space to reuse it", required = false)
    private String reuseSpaceSymbols;

    public static void main(String... args) {
        new LinkerScriptInjector().doMain(args);
    }

    public void doMain(String... args) {
        MainHelper.execute(args, this, () -> {
            final List<MappingItem> mapping = MappingReaderFactory.newReader(mappingFile).read();
            if (StringUtils.isNotEmpty(nameMappingFile)) {
                MappingUtil.addSymbolsFromNameMappingFile(mapping, nameMappingFile);
            }
            LinkerScriptWriterFactory.newWriter(linkerScriptFile).write(mapping, importSymbols, reuseSpaceSymbols);
        });
    }
}
