package ru.nami.hooktools.linker;

/**
 * Created by Dmitriy Dzhevaga on 24.12.2015.
 */
public enum LinkerScriptFormat {
    LSL,
    ILO;

    public static LinkerScriptFormat of(String name) {
        for (LinkerScriptFormat value : LinkerScriptFormat.class.getEnumConstants()) {
            if (value.name().compareToIgnoreCase(name) == 0) {
                return value;
            }
        }
        throw new IllegalArgumentException("Unknown file format: " + name);
    }
}
