package ru.nami.hooktools.linker;

import org.apache.commons.io.FilenameUtils;

/**
 * Created by Dmitriy Dzhevaga on 24.12.2015.
 */
public class LinkerScriptWriterFactory {
    private static final String UNKNOWN_FILE_FORMAT = "Unknown file format";

    public static LinkerScriptWriter newWriter(String file) {
        LinkerScriptFormat format = LinkerScriptFormat.of(FilenameUtils.getExtension(file));
        switch (format) {
            case ILO:
                return new IloWriter(file);
            case LSL:
                return new LslWriter(file);
        }
        throw new IllegalArgumentException(UNKNOWN_FILE_FORMAT);
    }
}
