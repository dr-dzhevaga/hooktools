package ru.nami.hooktools.linker;

import org.apache.commons.lang3.StringUtils;
import ru.nami.hooktools.mappping.MappingItem;
import ru.nami.hooktools.mappping.MappingUtil;

import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * Created by Dmitriy Dzhevaga on 18.11.2015.
 */
public class LslWriter extends LinkerScriptWriter {
    private static final String RESERVED_SECTION =
            "section_layout ::%s%n" +
                    "{%n" +
                    "   group (ordered, run_addr = 0x%X, page_size = 0, page)%n" +
                    "   {%n" +
                    "       reserved \"%s\" ( size = 0x%X );%n" +
                    "   }%n" +
                    "}%n";
    private static final String IMPORTED_SYMBOL =
            "section_layout ::%s%n" +
                    "{%n" +
                    "   group (ordered, run_addr = 0x%X)%n" +
                    "   {%n" +
                    "       select \"%s\";%n" +
                    "   }%n" +
                    "}%n";

    public LslWriter(String file) {
        super(file);
    }

    @Override
    protected String makeImportCommand(String name, String space, Long address, Byte bitNumber, Long length) {
        return String.format(IMPORTED_SYMBOL, space, address, name);
    }

    @Override
    protected String makeReserveCommand(String name, String space, Long address, Long length) {
        return String.format(RESERVED_SECTION, space, address, name, length);
    }

    @Override
    protected List<MappingItem> getSectionsToReserve(List<MappingItem> symbols, String importSymbols, String reuseSpaceSymbols) {
        boolean reuseSpace = StringUtils.isNotEmpty(reuseSpaceSymbols);
        Pattern reuseSpaceSymbolsPattern = reuseSpace ? Pattern.compile(reuseSpaceSymbols) : null;
        Pattern importSymbolsPattern = Pattern.compile(importSymbols);
        List<MappingItem> reservedSymbols = symbols.stream().
                filter(symbol -> !importSymbolsPattern.matcher(symbol.getName()).matches()).
                filter(symbol -> !reuseSpace || !reuseSpaceSymbolsPattern.matcher(symbol.getName()).matches()).
                collect(Collectors.toList());
        return MappingUtil.compress(reservedSymbols);
    }
}
