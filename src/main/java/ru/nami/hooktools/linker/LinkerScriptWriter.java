package ru.nami.hooktools.linker;

import ru.nami.hooktools.mappping.MappingItem;

import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * Created by Dmitriy Dzhevaga on 24.12.2015.
 */
public abstract class LinkerScriptWriter {
    private final String filePath;

    protected LinkerScriptWriter(String filePath) {
        this.filePath = filePath;
    }

    public void write(List<MappingItem> symbols, String importSymbols, String reuseSpaceSymbols) throws IOException {
        try (PrintWriter writer = new PrintWriter(Files.newBufferedWriter(Paths.get(filePath), StandardOpenOption.APPEND))) {
            writeReserved(writer, getSectionsToReserve(symbols, importSymbols, reuseSpaceSymbols));
            writeImported(writer, getSymbolsToImport(symbols, importSymbols));
        }
    }

    protected void writeReserved(PrintWriter printWriter, List<MappingItem> reservedSections) {
        int i = 1;
        for (MappingItem section : reservedSections) {
            printWriter.println();
            printWriter.print(makeReserveCommand("reserved_" + i, section.getSpace(), section.getAddress(), section.getLength()));
        }
    }

    protected void writeImported(PrintWriter printWriter, List<MappingItem> importSymbols) {
        importSymbols.forEach(symbol -> {
            printWriter.println();
            printWriter.print(makeImportCommand(symbol.getName(), symbol.getSpace(), symbol.getAddress(), symbol.getBitNumber(), symbol.getLength()));
        });
    }

    protected abstract List<MappingItem> getSectionsToReserve(List<MappingItem> symbols, String importSymbols, String reuseSpaceSymbols);

    protected abstract String makeReserveCommand(String name, String space, Long address, Long length);

    protected List<MappingItem> getSymbolsToImport(List<MappingItem> symbols, String importSymbols) {
        Pattern importSymbolsPattern = Pattern.compile(importSymbols);
        return symbols.stream().
                filter(symbol -> importSymbolsPattern.matcher(symbol.getName()).matches()).
                collect(Collectors.toList());
    }

    protected abstract String makeImportCommand(String name, String space, Long address, Byte bitNumber, Long length);
}
