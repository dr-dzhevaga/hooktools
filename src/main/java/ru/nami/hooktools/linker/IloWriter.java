package ru.nami.hooktools.linker;

import org.apache.commons.lang3.StringUtils;
import ru.nami.hooktools.mappping.MappingItem;
import ru.nami.hooktools.mappping.MappingUtil;

import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * Created by Dmitriy Dzhevaga on 24.12.2015.
 */
public class IloWriter extends LinkerScriptWriter {
    private static final String RESERVED_SECTION = "RESERVE(MEMORY(0x%X-0x%X))";
    private static final String IMPORTED_SYMBOL = "ASSIGN(%s(0x%X))";
    private static final String IMPORTED_BIT_SYMBOL = "ASSIGN(%s(0x%X.%d))";

    IloWriter(String file) {
        super(file);
    }

    @Override
    protected String makeImportCommand(String name, String space, Long address, Byte bitNumber, Long length) {
        if (bitNumber == null) {
            return String.format(IMPORTED_SYMBOL, name, address);
        } else {
            return String.format(IMPORTED_BIT_SYMBOL, name, address, bitNumber);
        }
    }

    @Override
    protected String makeReserveCommand(String name, String space, Long address, Long length) {
        return String.format(RESERVED_SECTION, address, address + length - 1);
    }

    @Override
    protected List<MappingItem> getSectionsToReserve(List<MappingItem> symbols, String importSymbols, String reuseSpaceSymbols) {
        boolean reuseSpace = StringUtils.isNotEmpty(reuseSpaceSymbols);
        Pattern reuseSpaceSymbolsPattern = reuseSpace ? Pattern.compile(reuseSpaceSymbols) : null;
        List<MappingItem> reservedSymbols = symbols.stream().
                filter(symbol -> !reuseSpace || !reuseSpaceSymbolsPattern.matcher(symbol.getName()).matches()).
                collect(Collectors.toList());
        return MappingUtil.compress(reservedSymbols);
    }
}
