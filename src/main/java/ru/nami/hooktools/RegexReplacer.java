package ru.nami.hooktools;

import org.kohsuke.args4j.Option;
import ru.nami.hooktools.util.MainHelper;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.IntStream;

/**
 * Created by Dmitriy Dzhevaga on 26.01.2016.
 */
public class RegexReplacer {
    @Option(name = "-i", aliases = "--input", usage = "specify input file", required = true)
    private String inputFile;

    @Option(name = "-o", aliases = "--output", usage = "specify output file", required = true)
    private String outputFile;

    @Option(name = "-l", aliases = "--line-pattern", usage = "regular expression to filter line to apply replacement", required = true)
    private Pattern linePattern;

    @Option(name = "-r", aliases = "--replace-pattern", usage = "regular expression to match line part to replace", required = true)
    private Pattern replacePattern;

    @Option(name = "-v", aliases = "--replace-value", usage = "replace value (use $0 to insert value matched by replace pattern)", required = false)
    private String replaceValue = "";

    public static void main(String... args) {
        new RegexReplacer().doMain(args);
    }

    public void doMain(String... args) {
        MainHelper.execute(args, this, () -> {
            List<String> lines = Files.readAllLines(Paths.get(inputFile));
            IntStream.range(0, lines.size()).forEach(i -> {
                String origin = lines.get(i);
                if (linePattern.matcher(origin).find()) {
                    String replaced = replacePattern.matcher(origin).replaceAll(replaceValue);
                    lines.set(i, replaced);
                    System.out.println("line " + i + " replacement: " + origin + " -> " + replaced);
                }
            });
            Files.write(Paths.get(outputFile), lines);
        });
    }
}
