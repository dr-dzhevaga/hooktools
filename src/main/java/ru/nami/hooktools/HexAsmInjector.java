package ru.nami.hooktools;

import org.kohsuke.args4j.Option;
import ru.nami.hooktools.asm.AsmInjector;
import ru.nami.hooktools.hex.IntelHexReader;
import ru.nami.hooktools.hex.IntelHexWriter;
import ru.nami.hooktools.mappping.MappingItem;
import ru.nami.hooktools.mappping.MappingReaderFactory;
import ru.nami.hooktools.util.MainHelper;

import java.util.Arrays;
import java.util.List;
import java.util.SortedMap;

/**
 * Created by Dmitriy Dzhevaga on 20.11.2015.
 */
public class HexAsmInjector {
    @Option(name = "-oh", aliases = "--origin-hex", usage = "specify origin hex file", required = true)
    private String originHexFile;

    @Option(name = "-om", aliases = "--origin-mapping", usage = "specify origin mapping file", required = true)
    private String originMappingFile;

    @Option(name = "-hm", aliases = "--hook-mapping", usage = "specify hook mapping file", required = true)
    private String hookMappingFile;

    @Option(name = "-hs", aliases = "--hook-symbols", usage = "specify comma-separated list of symbols to hook", required = true)
    private String symbolsToHook;

    public static void main(String... args) {
        new HexAsmInjector().doMain(args);
    }

    public void doMain(String... args) {
        MainHelper.execute(args, this, () -> {
            final SortedMap<Long, Integer> originMemory = IntelHexReader.read(originHexFile);
            final List<MappingItem> originMapping = MappingReaderFactory.newReader(originMappingFile).read();
            final List<MappingItem> hookMapping = MappingReaderFactory.newReader(hookMappingFile).read();
            AsmInjector.updateCALLS(originMemory, originMapping, hookMapping, Arrays.asList(symbolsToHook.split(",")));
            IntelHexWriter.write(originHexFile, originMemory);
        });
    }
}