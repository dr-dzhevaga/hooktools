package ru.nami.hooktools.util;

import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;

/**
 * Created by Dmitriy Dzhevaga on 26.11.2015.
 */
public class MainHelper {
    private MainHelper() {
    }

    public static void execute(String[] args, Object bean, Main main) {
        CmdLineParser cmdLineParser = new CmdLineParser(bean);
        try {
            cmdLineParser.parseArgument(args);
            main.run();
        } catch (CmdLineException e) {
            System.err.println(e.getMessage());
            cmdLineParser.printUsage(System.out);
            System.exit(1);
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(1);
        }
        System.exit(0);
    }

    @FunctionalInterface
    public interface Main {
        void run() throws Exception;
    }
}
