package ru.nami.hooktools.util;

import java.util.SortedMap;

/**
 * Created by Dmitriy Dzhevaga on 20.11.2015.
 */
public class MemoryMerger {

    private static final String OVERLAP_ERROR = "Conflict at address: 0x%X. Origin value: 0x%X. New value: 0x%X.";

    private MemoryMerger() {
    }

    public static void merge(SortedMap<Long, Integer> origin, SortedMap<Long, Integer> patch, MergeMode mode, boolean silentMode) {
        patch.forEach((address, value) -> {
            if (origin.containsKey(address)) {
                switch (mode) {
                    case KEEP:
                        if (!silentMode || !origin.get(address).equals(0)) {
                            System.out.print(String.format(OVERLAP_ERROR, address, origin.get(address), value));
                            System.out.println(" Keep origin value.");
                        }
                        break;
                    case OVERRIDE:
                        if (!silentMode || !origin.get(address).equals(0)) {
                            System.out.print(String.format(OVERLAP_ERROR, address, origin.get(address), value));
                            System.out.println(" Override origin value.");
                        }
                        origin.put(address, value);
                        break;
                    case THROW:
                        throw new RuntimeException(String.format(OVERLAP_ERROR, address, origin.get(address), value));
                }
            } else {
                origin.put(address, value);
            }
        });
    }

    public enum MergeMode {
        KEEP,
        OVERRIDE,
        THROW;

        public static MergeMode of(String string) {
            for (MergeMode mergeMode : MergeMode.class.getEnumConstants()) {
                if (mergeMode.toString().toLowerCase().equals(string)) {
                    return mergeMode;
                }
            }
            throw new IllegalArgumentException("Unknown merge mode: " + string);
        }
    }
}
