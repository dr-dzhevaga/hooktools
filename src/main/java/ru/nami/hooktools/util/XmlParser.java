package ru.nami.hooktools.util;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dmitriy Dzhevaga on 18.11.2015.
 */
public class XmlParser {
    private final Document xmlDocument;

    private XmlParser(String path) throws ParserConfigurationException, IOException, SAXException {
        DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = builderFactory.newDocumentBuilder();
        this.xmlDocument = builder.parse(path);
    }

    public static XmlParser parse(String path) throws IOException, SAXException, ParserConfigurationException {
        return new XmlParser(path);
    }

    public List<String> getTextContent(String xpath) throws XPathExpressionException {
        final XPath xPath = XPathFactory.newInstance().newXPath();
        final NodeList nodes = (NodeList) xPath.compile(xpath).evaluate(xmlDocument, XPathConstants.NODESET);
        final List<String> result = new ArrayList<>(nodes.getLength());
        for (int i = 0; i < nodes.getLength(); i++) {
            result.add(nodes.item(i).getTextContent());
        }
        return result;
    }
}
