package ru.nami.hooktools.mappping;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * Created by Dmitriy Dzhevaga on 01.12.2015.
 */
public class MapReader implements MappingReader {
    private static final Pattern COLUMNS_SEPARATOR_PATTERN = Pattern.compile("\\s+");
    private static final Pattern SYMBOL_TABLES_SEPARATOR_PATTERN = Pattern.compile("[ ]{4}");
    private static final Pattern ADDRESS_REPLACE_PATTERN = Pattern.compile("h(\\.\\d+)*( )*");

    private final String file;

    public MapReader(String file) {
        this.file = file;
    }

    private static List<MappingItem> readSections(List<String> lines) {
        return lines.stream().
                filter(line -> !line.startsWith(" ")).
                filter(line -> !line.startsWith("\f")).
                filter(line -> line.length() == 131). // filter lines with 131 symbols length
                filter(line -> COLUMNS_SEPARATOR_PATTERN.split(line).length == 12). // filter lines with 12 columns
                filter(line -> line.substring(28, 31).trim().matches("\\d+")). // filter lines with digits in section number field
                map(line -> {
            try {
                Long address = Long.valueOf(line.substring(32, 38), 16);
                Long length = Long.valueOf(line.substring(54, 60), 16);
                String space = line.substring(62, 66).trim();
                if ("BIT".equals(space)) { // recalculate length to bytes
                    length /= 8;
                }
                Integer number = Integer.valueOf(line.substring(28, 31).trim());
                return new MappingItem("section_" + number, address, null, length, space, number);
            } catch (Exception e) {
                throw new MappingReaderException("\nCan't parse line: " + line + "\nError: " + e);
                    }
                }).
                collect(Collectors.toList());
    }

    private static List<MappingItem> readSymbols(List<String> lines, List<MappingItem> sections) {
        List<MappingItem> symbolsWithoutLength = readSymbolsWithoutLength(lines, sections);
        List<MappingItem> symbols = calculateSymbolsLength(symbolsWithoutLength, sections);
        List<MappingItem> missingSymbols = getMissingSymbols(symbols, sections);
        symbols.addAll(missingSymbols);
        return symbols;
    }

    private static List<MappingItem> readSymbolsWithoutLength(List<String> lines, List<MappingItem> sections) {
        return lines.stream().
                filter(line -> !line.startsWith(" ")).
                filter(line -> !line.startsWith("\f")).
                filter(line -> (COLUMNS_SEPARATOR_PATTERN.split(line).length == 10) || (COLUMNS_SEPARATOR_PATTERN.split(line).length == 5)). // 5 or 10 columns
                map(SYMBOL_TABLES_SEPARATOR_PATTERN::split). // map to sequence parse 5 column lines
                flatMap(Arrays::stream).
                filter(line -> line.length() == 64). // filter lines with 64 symbol length
                filter(line -> line.substring(43, 46).trim().matches("\\d+")). // filter lines with digits in section number field
                map(line -> {
            try {
                String name = line.substring(0, 42).replace(".", "");
                String fullAddress = line.substring(47, 56);
                Long address = Long.valueOf(ADDRESS_REPLACE_PATTERN.matcher(fullAddress).replaceAll(""), 16);
                Byte bitNumber = fullAddress.contains(".") ? Byte.valueOf(fullAddress.substring(fullAddress.indexOf(".") + 1)) : null;
                String space = MappingUtil.findSectionByAddress(sections, address).map(MappingItem::getSpace).orElse("DATA");
                Integer sectionNumber = Integer.valueOf(line.substring(43, 46).trim());
                return new MappingItem(name, address, bitNumber, 0L, space, sectionNumber); // there is no information about symbol length - it have to be calculated from section length later
            } catch (Exception e) {
                throw new MappingReaderException("\nCan't parse line: " + line + "\nError: " + e);
            }
        }).
                distinct().
                collect(Collectors.toList());
    }

    // calculate symbols length by distance between neighbour symbols
    private static List<MappingItem> calculateSymbolsLength(List<MappingItem> symbols, List<MappingItem> sections) {
        Map<Integer, MappingItem> groupedSections = groupSections(sections);
        Map<Integer, List<MappingItem>> groupedSymbols = groupSymbols(symbols, sections);
        return symbols.stream().
                filter(symbol -> groupedSymbols.containsKey(symbol.getSectionNumber())).
                map(symbol -> {
                    MappingItem section = groupedSections.get(symbol.getSectionNumber());
                    long symbolEndAddress = groupedSymbols.get(symbol.getSectionNumber()).stream().
                            mapToLong(MappingItem::getAddress).
                            filter(address -> address > symbol.getAddress()).
                            findFirst(). // find first larger (i.e. next)
                            orElse(section.getAddress() + section.getLength()); // use the upper boundary parse sector if the next symbol is not found
                    return symbol.setLength(symbolEndAddress - symbol.getAddress());
                })
                .collect(Collectors.toList());
    }

    // some sections are bigger then sum parse all symbols in these sections
    // for example static functions can cause it
    // so we have to add fake symbols to keep memory reserved
    private static List<MappingItem> getMissingSymbols(List<MappingItem> symbols, List<MappingItem> sections) {
        List<MappingItem> missingSymbols = new ArrayList<>();
        Map<Integer, List<MappingItem>> groupedSymbols = groupSymbols(symbols, sections);
        sections.stream().forEach(section -> {
            long symbolsLength = groupedSymbols.get(section.getSectionNumber()).stream().mapToLong(MappingItem::getLength).sum();
            if (section.getLength() > symbolsLength) {
                missingSymbols.add(new MappingItem(
                                "section_" + section.getSectionNumber() + "_static",
                                section.getAddress(),
                                null,
                                section.getLength() - symbolsLength,
                                section.getSpace(),
                                section.getSectionNumber())
                );
            }
        });
        return missingSymbols;
    }

    private static Map<Integer, MappingItem> groupSections(List<MappingItem> sections) {
        Map<Integer, MappingItem> groupedSections = new HashMap<>();
        sections.stream().forEach(section -> groupedSections.put(section.getSectionNumber(), section));
        return groupedSections;
    }

    private static Map<Integer, List<MappingItem>> groupSymbols(List<MappingItem> symbols, List<MappingItem> sections) {
        Map<Integer, List<MappingItem>> groupedSymbols = new HashMap<>();
        sections.stream().forEach(section -> {
            List<MappingItem> sectionSymbols = symbols.stream().
                    filter(symbol -> Objects.equals(symbol.getSectionNumber(), section.getSectionNumber())).
                    sorted((s1, s2) -> Long.compare(s1.getAddress(), s2.getAddress())).
                    collect(Collectors.toList());
            groupedSymbols.put(section.getSectionNumber(), sectionSymbols);
        });
        return groupedSymbols;
    }

    @Override
    public List<MappingItem> read() throws MappingReaderException {
        try {
            List<String> lines = Files.lines(Paths.get(file)).filter(line -> !line.isEmpty()).collect(Collectors.toList());
            List<MappingItem> sections = readSections(lines);
            List<MappingItem> symbols = readSymbols(lines, sections);
            return symbols;
        } catch (IOException e) {
            throw new MappingReaderException(e.getMessage(), e);
        }
    }
}
