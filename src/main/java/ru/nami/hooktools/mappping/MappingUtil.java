package ru.nami.hooktools.mappping;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Created by Dmitriy Dzhevaga on 23.11.2015.
 */
public class MappingUtil {

    private MappingUtil() {
    }

    public static Optional<MappingItem> findSymbolByName(List<MappingItem> mapping, String symbolName) {
        return mapping.stream().
                filter(mappingSymbol -> symbolName.equals(mappingSymbol.getName())).
                findFirst();
    }

    public static MappingItem getSymbolByName(List<MappingItem> mapping, String symbolName) {
        return findSymbolByName(mapping, symbolName).orElseThrow(() -> new RuntimeException("Symbol " + symbolName + " is not found in mapping"));
    }

    public static Optional<MappingItem> findSectionByAddress(List<MappingItem> sections, long address) {
        return sections.stream().
                filter(section -> address >= section.getAddress() && address < section.getAddress() + section.getLength()).
                findFirst();
    }

    public static MappingItem getSectionByAddress(List<MappingItem> sections, long address) {
        return findSectionByAddress(sections, address).orElseThrow(() -> new RuntimeException(String.format("Address 0x%X is not found in any section", address)));
    }

    public static List<MappingItem> compress(List<? extends MappingItem> sections) {
        sections.sort((s1, s2) -> Long.compare(s1.getAddress(), s2.getAddress()));
        List<MappingItem> filledSections = new ArrayList<>();
        long startAddress = sections.get(0).getAddress();
        long currentAddress = startAddress;
        String space = sections.get(0).getSpace();
        for (MappingItem symbol : sections) {
            if (symbol.getAddress() >= currentAddress) {
                if (currentAddress != symbol.getAddress() || !space.equals(symbol.getSpace())) {
                    filledSections.add(new MappingItem("compressed_section", startAddress, null, currentAddress - startAddress, space, 0));
                    startAddress = symbol.getAddress();
                    currentAddress = startAddress;
                    space = symbol.getSpace();
                }
                currentAddress += symbol.getLength();
            }
        }
        filledSections.add(new MappingItem("compressed_section", startAddress, null, currentAddress - startAddress, space, 0));
        return filledSections;
    }

    public static void addSymbolsFromNameMappingFile(List<MappingItem> symbols, String nameMappingFile) throws IOException {
        RenamingDictionary.read(nameMappingFile).forEach((oldName, newNames) -> {
            Optional<MappingItem> symbol = MappingUtil.findSymbolByName(symbols, oldName);
            if (symbol.isPresent()) {
                newNames.forEach(newName -> symbols.add(symbol.get().setName(newName)));
            }
        });
    }
}
