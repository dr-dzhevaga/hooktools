package ru.nami.hooktools.mappping;

import java.util.List;

/**
 * Created by Dmitriy Dzhevaga on 07.12.2015.
 */
public interface MappingReader {
    List<MappingItem> read() throws MappingReaderException;
}
