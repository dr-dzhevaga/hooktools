package ru.nami.hooktools.mappping;

import org.xml.sax.SAXException;
import ru.nami.hooktools.util.XmlParser;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Dmitriy Dzhevaga on 18.11.2015.
 */
public class MapxmlReader implements MappingReader {
    private static final String LOCATE_RULES_XPATH = "/mapfile/part[./name/text() = 'Locate Rules']/table/row";
    private static final String SECTIONS_XPATH = "/mapfile/part[./name/text() = 'Locate Result']/part[./name/text() = 'Sections']/table/row";
    private static final String ROW_SEPARATOR = ";";
    private static final String NAME_REMOVE_NUMBER_REGEX = " \\(\\d+\\)";
    private static final String NAME_REMOVE_SPACE_REGEX = "(bit|bita|iram|near|far|shuge|huge|code)";
    private static final String LOCATE_RULES_WRONG_PARAMETERS_NUMBER_ERROR = "Unexpected parameters number in locate rules table: ";
    private static final String SECTIONS_WRONG_PARAMETERS_NUMBER_ERROR = "Unexpected parameters number in sections table: ";

    private final String file;

    public MapxmlReader(String file) {
        this.file = file;
    }

    private static List<MappingItem> readSymbols(XmlParser mappingParser, Map<String, String> locateRulesMap) throws XPathExpressionException {
        final List<String> locateResult = mappingParser.getTextContent(SECTIONS_XPATH);
        final List<MappingItem> mapping = new ArrayList<>();
        locateResult.stream().map(row -> row.split(ROW_SEPARATOR)).forEach(parameters -> {
            if (parameters.length != 7) {
                throw new MappingReaderException(SECTIONS_WRONG_PARAMETERS_NUMBER_ERROR + parameters.length);
            }
            String name = parameters[2].replaceFirst(NAME_REMOVE_NUMBER_REGEX, "").replaceFirst(NAME_REMOVE_SPACE_REGEX, "");
            String addressSpace = locateRulesMap.get(name);
            Long address = Long.valueOf(parameters[4].substring(2), 16);
            Byte bitNumber = null; // TODO: implement bit number parsing
            Long length = Long.valueOf(parameters[3].substring(2), 16);
            Integer sectionNumber = 0; // section number is not used in any calculation so let's keep it 0 to simplify
            MappingItem symbol = new MappingItem(name, address, bitNumber, length, addressSpace, sectionNumber);
            mapping.add(symbol);
        });
        return mapping;
    }

    private static Map<String, String> readSpaceMapping(XmlParser xmlParser) throws ParserConfigurationException, SAXException, IOException, XPathExpressionException {
        final List<String> locateRules = xmlParser.getTextContent(LOCATE_RULES_XPATH);
        final Map<String, String> locateRulesMap = new HashMap<>();
        locateRules.stream().map(row -> row.split(ROW_SEPARATOR)).forEach(parameters -> {
            if (parameters.length != 5) {
                throw new MappingReaderException(LOCATE_RULES_WRONG_PARAMETERS_NUMBER_ERROR + parameters.length);
            }
            String space = parameters[0];
            String[] names = parameters[4].split(" , ");
            for (String name : names) {
                locateRulesMap.put(name.replaceFirst(NAME_REMOVE_NUMBER_REGEX, "").replaceFirst(NAME_REMOVE_SPACE_REGEX, ""), space);
            }
        });
        return locateRulesMap;
    }

    @Override
    public List<MappingItem> read() throws MappingReaderException {
        try {
            XmlParser mappingParser = XmlParser.parse(file);
            Map<String, String> spaceMapping = readSpaceMapping(mappingParser);
            List<MappingItem> symbols = readSymbols(mappingParser, spaceMapping);
            return symbols;
        } catch (SAXException | ParserConfigurationException | XPathExpressionException | IOException e) {
            throw new MappingReaderException(e.getMessage(), e);
        }
    }
}
