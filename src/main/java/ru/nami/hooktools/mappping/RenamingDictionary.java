package ru.nami.hooktools.mappping;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

/**
 * Created by Dmitriy Dzhevaga on 27.01.2016.
 */
public class RenamingDictionary {
    private static final List<String> excludeList = Arrays.asList("notused", "tobereplaced", "tobedeleted");

    private RenamingDictionary() {
    }

    public static Map<String, List<String>> read(String nameMappingFile) throws IOException {
        Map<String, List<String>> nameMapping = new HashMap<>();
        Files.lines(Paths.get(nameMappingFile)).
                map(line -> line.split("\\s+")).
                filter(columns -> columns.length == 2).
                filter(columns -> !columns[0].isEmpty() && !columns[1].isEmpty()).
                filter(columns -> !excludeList.contains(columns[0]) && !excludeList.contains(columns[1])).
                forEach(columns -> {
                    String newName = "_" + columns[0];
                    String oldName = "_" + columns[1];
                    List<String> newNames = nameMapping.getOrDefault(oldName, new ArrayList<>());
                    newNames.add(newName);
                    nameMapping.put(oldName, newNames);
                });
        return nameMapping;
    }
}
