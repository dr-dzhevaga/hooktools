package ru.nami.hooktools.mappping;

import java.util.Objects;

/**
 * Created by Dmitriy Dzhevaga on 18.11.2015.
 */
public class MappingItem {
    private final String name;
    private final String space;
    private final Long address;
    private final Byte bitNumber;
    private final Long length;
    private final Integer sectionNumber;

    public MappingItem(String name, Long address, Byte bitNumber, Long length, String space, Integer sectionNumber) {
        this.name = name;
        this.address = address;
        this.bitNumber = bitNumber;
        this.length = length;
        this.space = space;
        this.sectionNumber = sectionNumber;
    }

    public String getName() {
        return name;
    }

    public MappingItem setName(String name) {
        return new MappingItem(name, getAddress(), bitNumber, getLength(), getSpace(), getSectionNumber());
    }

    public Long getAddress() {
        return address;
    }

    public Byte getBitNumber() {
        return bitNumber;
    }

    public Long getLength() {
        return length;
    }

    public MappingItem setLength(long length) {
        return new MappingItem(getName(), getAddress(), bitNumber, length, getSpace(), getSectionNumber());
    }

    public String getSpace() {
        return space;
    }

    public Integer getSectionNumber() {
        return sectionNumber;
    }

    @Override
    public String toString() {
        String formatted = getName() + ": 0x" + Long.toHexString(getAddress());
        if (getBitNumber() != null) {
            formatted += "." + getBitNumber();
        }
        return formatted;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof MappingItem)) {
            return false;
        }
        MappingItem other = (MappingItem) obj;
        // ignore name field
        return Objects.equals(this.getAddress(), other.getAddress()) &&
                Objects.equals(this.getLength(), other.getLength()) &&
                Objects.equals(this.getBitNumber(), other.getBitNumber()) &&
                Objects.equals(this.getSpace(), other.getSpace()) &&
                Objects.equals(this.getSectionNumber(), other.getSectionNumber());
    }

    @Override
    public int hashCode() {
        // ignore name field
        return Objects.hash(getAddress(), getBitNumber(), getLength(), getSpace(), getSectionNumber());
    }
}
