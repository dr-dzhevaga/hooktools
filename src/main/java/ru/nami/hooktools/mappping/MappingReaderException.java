package ru.nami.hooktools.mappping;

/**
 * Created by Dmitriy Dzhevaga on 17.11.2015.
 */
public class MappingReaderException extends RuntimeException {
    public MappingReaderException(String message) {
        super(message);
    }

    public MappingReaderException(String message, Throwable throwable) {
        super(message, throwable);
    }
}
