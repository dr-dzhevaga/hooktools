package ru.nami.hooktools.mappping;

/**
 * Created by Dmitriy Dzhevaga on 07.12.2015.
 */
public enum MappingFormat {
    MAP,
    MAPXML;

    public static MappingFormat of(String name) {
        for (MappingFormat value : MappingFormat.class.getEnumConstants()) {
            if (value.name().compareToIgnoreCase(name) == 0) {
                return value;
            }
        }
        throw new IllegalArgumentException("Unknown file format: " + name);
    }
}
