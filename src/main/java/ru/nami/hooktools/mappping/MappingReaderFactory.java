package ru.nami.hooktools.mappping;

import org.apache.commons.io.FilenameUtils;

/**
 * Created by Dmitriy Dzhevaga on 07.12.2015.
 */
public class MappingReaderFactory {
    private static final String UNKNOWN_FILE_FORMAT = "Unknown file format";

    public static MappingReader newReader(String file) {
        MappingFormat format = MappingFormat.of(FilenameUtils.getExtension(file));
        switch (format) {
            case MAP:
                return new MapReader(file);
            case MAPXML:
                return new MapxmlReader(file);
        }
        throw new IllegalArgumentException(UNKNOWN_FILE_FORMAT);
    }
}
