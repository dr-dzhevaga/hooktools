package ru.nami.hooktools.hex;

import java.util.Arrays;
import java.util.stream.IntStream;

/**
 * Created by Dmitriy Dzhevaga on 19.11.2015.
 */
public class IntelHexRecord {
    private final IntelHexRecordType type;
    private final long address;
    private final int[] data;

    public IntelHexRecord(IntelHexRecordType type, long address, int[] data) {
        this.type = type;
        this.address = address;
        this.data = data;
    }

    public IntelHexRecord(String record) throws IntelHexException {
        if (record.charAt(0) != ':') {
            throw new IntelHexException("Illegal first symbol: " + record.charAt(0) + ", ':' is expected");
        }
        if (!checkCRC(record)) {
            throw new IntelHexException("Record crc error: " + record);
        }
        int length = getInteger(record, 1, 2);
        if (length * 2 + 11 != record.length()) {
            throw new IntelHexException("Illegal record length: " + record.length() + ", " + length + 11 + " is expected");
        }
        int address = getInteger(record, 3, 4);
        int typeCode = getInteger(record, 7, 2);
        IntelHexRecordType type = IntelHexRecordType.of(typeCode);
        int[] data = IntStream.range(0, length).map(i -> getInteger(record, 9 + i * 2, 2)).toArray();
        this.type = type;
        this.address = address;
        this.data = data;
    }

    public static IntelHexRecord parse(String record) throws IntelHexException {
        return new IntelHexRecord(record);
    }

    private static boolean checkCRC(String record) {
        String checked = record.substring(1);
        int crc = IntStream.range(0, checked.length() / 2).map(i -> getInteger(checked, i * 2, 2)).sum();
        return (crc & 0xFF) == 0;
    }

    private static int getInteger(String string, int start, int length) {
        return Integer.parseUnsignedInt(string.substring(start, start + length), 16);
    }

    public long getSegmentAddress() {
        if (type != IntelHexRecordType.SEGMENT_ADDRESS) {
            throw new IllegalStateException("Not a segment address record");
        }
        return (data[0] << 12) | (data[1] << 4);
    }

    public long getLinearAddress() {
        if (type != IntelHexRecordType.LINEAR_ADDRESS) {
            throw new IllegalStateException("Not a linear address record");
        }
        return (data[0] << 24) | (data[1] << 16);
    }

    public IntelHexRecordType getType() {
        return type;
    }

    public long getAddress() {
        return address;
    }

    public int[] getData() {
        return data;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder(11 + data.length * 2).
                append(":").
                append(String.format("%02X", data.length)).
                append(String.format("%04X", address)).
                append(String.format("%02X", type.toCode()));
        Arrays.stream(data).mapToObj(d -> String.format("%02X", d)).forEach(builder::append);
        builder.append(String.format("%02X", getCRC()));
        return builder.toString();
    }

    private int getCRC() {
        long sum = data.length
                + type.toCode()
                + ((address >> 8) & 0xFF)
                + (address & 0xFF)
                + Arrays.stream(data).sum();
        return (int) ((0x100 - (sum & 0xFF)) & 0xFF);
    }
}
