package ru.nami.hooktools.hex;

import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;

/**
 * Created by Dmitriy Dzhevaga on 19.11.2015.
 */
public class IntelHexWriter {
    private IntelHexWriter() {
    }

    public static void write(String filePath, SortedMap<Long, Integer> memory) throws IOException {
        try (PrintWriter writer = new PrintWriter(Files.newBufferedWriter(Paths.get(filePath)))) {
            long offset = 0;
            long linearAddress = 0;
            List<Integer> data = new ArrayList<>(32);
            for (Map.Entry<Long, Integer> memoryEntry : memory.entrySet()) {
                long memoryAddress = memoryEntry.getKey();
                if (memoryAddress != offset + linearAddress + data.size()) {
                    if (data.size() > 0) {
                        writer.println(makeDataRecord(linearAddress, data));
                        data.clear();
                    }
                    long newOffset = memoryAddress & 0xFFFF0000;
                    if (newOffset != offset) {
                        offset = newOffset;
                        writer.println(makeLinearAddressRecord(newOffset));
                    }
                    linearAddress = memoryAddress & 0x0000FFFF;
                }
                if (data.size() >= 32) {
                    writer.println(makeDataRecord(linearAddress, data));
                    linearAddress += data.size();
                    data.clear();
                    if (linearAddress >= 0xFFFF) {
                        long absoluteAddress = offset + linearAddress;
                        linearAddress = absoluteAddress & 0x0000FFFF;
                        offset = absoluteAddress & 0xFFFF0000;
                        writer.println(makeLinearAddressRecord(offset));
                    }
                }
                data.add(memoryEntry.getValue());
            }
            writer.println(makeDataRecord(linearAddress, data));
            writer.print(makeEndRecord());
        }
    }

    private static IntelHexRecord makeLinearAddressRecord(long linearAddress) {
        int[] data = new int[]{(int) (linearAddress >> 24) & 0xFF, (int) (linearAddress >> 16) & 0xFF};
        return new IntelHexRecord(IntelHexRecordType.LINEAR_ADDRESS, 0, data);
    }

    private static IntelHexRecord makeDataRecord(long address, List<Integer> data) {
        return new IntelHexRecord(IntelHexRecordType.DATA, address, data.stream().mapToInt(Integer::intValue).toArray());
    }

    private static IntelHexRecord makeEndRecord() {
        return new IntelHexRecord(IntelHexRecordType.EOF, 0, new int[0]);
    }
}
