package ru.nami.hooktools.hex;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;

/**
 * Created by Dmitriy Dzhevaga on 19.11.2015.
 */
public class IntelHexReader {
    private IntelHexReader() {
    }

    public static SortedMap<Long, Integer> read(String filePath) throws IOException, IntelHexException {
        SortedMap<Long, Integer> memory = new TreeMap<>();
        List<String> lines = Files.readAllLines(Paths.get(filePath));
        long offset = 0;
        for (String line : lines) {
            IntelHexRecord record = IntelHexRecord.parse(line);
            switch (record.getType()) {
                case SEGMENT_ADDRESS:
                    offset = record.getSegmentAddress();
                    break;
                case LINEAR_ADDRESS:
                    offset = record.getLinearAddress();
                    break;
                case DATA:
                    long address = record.getAddress();
                    for (int data : record.getData()) {
                        memory.put(offset + address++, data);
                    }
                    break;
                case EOF:
                    return memory;
            }
        }
        return memory;
    }
}
