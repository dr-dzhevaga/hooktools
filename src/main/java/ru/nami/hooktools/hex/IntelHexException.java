package ru.nami.hooktools.hex;

/**
 * Created by Dmitriy Dzhevaga on 19.11.2015.
 */
public class IntelHexException extends Exception {
    public IntelHexException(String message) {
        super(message);
    }

    public IntelHexException(String message, Throwable throwable) {
        super(message, throwable);
    }
}
