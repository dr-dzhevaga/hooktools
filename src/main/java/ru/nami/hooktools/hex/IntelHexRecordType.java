package ru.nami.hooktools.hex;

/**
 * Created by Dmitriy Dzhevaga on 19.11.2015.
 */
public enum IntelHexRecordType {
    DATA(0),
    EOF(1),
    SEGMENT_ADDRESS(3),
    START_SEGMENT_ADDRESS(3),
    LINEAR_ADDRESS(4),
    START_LINEAR_ADDRESS(5);

    private final int code;

    IntelHexRecordType(int code) {
        this.code = code;
    }

    public static IntelHexRecordType of(int code) {
        for (IntelHexRecordType type : IntelHexRecordType.class.getEnumConstants()) {
            if (type.toCode() == code) {
                return type;
            }
        }
        throw new IllegalArgumentException("Unknown hex record type: " + String.valueOf(code));
    }

    public int toCode() {
        return code;
    }
}
