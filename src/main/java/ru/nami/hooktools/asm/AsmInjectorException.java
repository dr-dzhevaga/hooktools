package ru.nami.hooktools.asm;

/**
 * Created by Dmitriy Dzhevaga on 20.11.2015.
 */
public class AsmInjectorException extends Exception {
    public AsmInjectorException(String message) {
        super(message);
    }

    public AsmInjectorException(String message, Throwable throwable) {
        super(message, throwable);
    }
}
