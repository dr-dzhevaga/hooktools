package ru.nami.hooktools.asm;

import ru.nami.hooktools.mappping.MappingItem;
import ru.nami.hooktools.mappping.MappingUtil;

import java.util.List;
import java.util.Objects;
import java.util.SortedMap;
import java.util.stream.Collectors;

/**
 * Created by Dmitriy Dzhevaga on 20.11.2015.
 */
public class AsmInjector {
    private static final Integer CALLS_CODE = 0xDA;
    private static final Integer JMPS_CODE = 0xFA;

    private AsmInjector() {
    }

    public static void updateCALLS(SortedMap<Long, Integer> memory, List<MappingItem> originMapping, List<MappingItem> hookMapping, List<String> symbolsToHook) throws AsmInjectorException {
        for (String hookSymbol : symbolsToHook) {
            long originAddress = MappingUtil.getSymbolByName(originMapping, hookSymbol).getAddress();
            int originSegment = getSegment(originAddress);
            int originAddressLowByte = getAddressLowByte(originAddress);
            int originAddressHighByte = getAddressHighByte(originAddress);
            long hookAddress = MappingUtil.getSymbolByName(hookMapping, hookSymbol).getAddress();
            int hookSegment = getSegment(hookAddress);
            int hookAddressLowByte = getAddressLowByte(hookAddress);
            int hookAddressHighByte = getAddressHighByte(hookAddress);
            try {
                getCallCommandAddress(memory, originSegment, originAddressLowByte, originAddressHighByte).forEach(address -> {
                    memory.put(address + 1, hookSegment);
                    memory.put(address + 2, hookAddressLowByte);
                    memory.put(address + 3, hookAddressHighByte);
                    System.out.println(hookSymbol + " is hooked from 0x" + Long.toHexString(originAddress) + " to 0x" + Long.toHexString(hookAddress));
                });
            } catch (AsmInjectorException e) {
                throw new AsmInjectorException(String.format("Can not hook %s symbol: %s", hookSymbol, e.getMessage()));
            }
        }
    }

    private static List<Long> getCallCommandAddress(SortedMap<Long, Integer> memory, Integer callSegment, Integer callAddressLowByte, Integer callAddressHighByte) throws AsmInjectorException {
        List<Long> commandAddress = memory.keySet().stream().
                filter(address -> (address % 2) == 0).
                filter(address -> Objects.equals(memory.get(address), CALLS_CODE) || Objects.equals(memory.get(address), JMPS_CODE)).
                filter(address -> Objects.equals(memory.get(address + 1), callSegment)).
                filter(address -> Objects.equals(memory.get(address + 2), callAddressLowByte)).
                filter(address -> Objects.equals(memory.get(address + 3), callAddressHighByte)).
                collect(Collectors.toList());
        if (commandAddress.size() == 0) {
            throw new AsmInjectorException(String.format("CALLS or JMPS to 0x%X%X%X not found", callSegment, callAddressHighByte, callAddressLowByte));
        }
        return commandAddress;
    }

    private static int getSegment(long address) {
        return ((int) (address >> 16)) & 0xFF;
    }

    private static int getAddressHighByte(long address) {
        return ((int) (address >> 8)) & 0xFF;
    }

    private static int getAddressLowByte(long address) {
        return ((int) address) & 0xFF;
    }
}
